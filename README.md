```
.
├── cdc_cons_gen
│   ├── find_clk_rst_prots_in_verdi_ports.py
│   ├── gen_cons.py
│   ├── gen_cons_utils.py
│   ├── ports2yml.py
│   ├── __pycache__
│   │   ├── gen_cons_utils.cpython-310.pyc
│   │   └── gen_cons_utils.cpython-36.pyc
│   ├── tests
│   │   ├── cons_crg.tcl
│   │   ├── cons_grid_template.txt
│   │   ├── crg_gen_xls_out.yml
│   │   ├── ports_info
│   │   │   └── mdl_a.ports
│   │   ├── tmp.tcl
│   │   └── verdi_get_mod_io_main.ports
│   └── xls2yml.py
├── cdc_cons_history
│   ├── cdc_tcl2yml.py
│   ├── README.md
│   ├── tests
│   │   └── cdc.tcl
│   └── try_es_idx_kib.txt
├── cdc_cons_ref
│   ├── cdc_tcl2yml.py
│   ├── idx_cdc_cons_history.json
│   ├── mock_bes_cons_clk.tcl
│   ├── README.md
│   ├── tests
│   │   └── cdc.tcl
│   ├── try_es_idx_kib.txt
│   └── t.yml
├── cdc_cons_visual
│   ├── demo.tcl
│   └── parser.py
├── cdc_es
│   ├── bes_cons2clk_grp.py
│   ├── __pycache__
│   │   ├── es_connect.cpython-310.pyc
│   │   └── rpt2json.cpython-310.pyc
│   ├── README.md
│   ├── rpt2json.py
│   ├── tests
│   │   ├── input
│   │   │   └── mock.rpt
│   │   └── output
│   │       └── mock.json
│   ├── try_es_idx_kib.txt
│   └── which_module.py
├── cdc_mdl2top
│   ├── cdc_es_status
│   │   ├── cfg2es.py
│   │   ├── doc2es.py
│   │   ├── __pycache__
│   │   │   └── cfg2es.cpython-36.pyc
│   │   ├── tests
│   │   │   └── input_filter_one_line.tcl
│   │   ├── tmp.json
│   │   └── try_es_idx_kib.txt
│   ├── eda_status_filter_util.py
│   ├── exc_query
│   │   ├── apply_do_fi_on_vio.py
│   │   ├── dash_cdc
│   │   │   └── index.py
│   │   ├── do_fi_to_es_body.py
│   │   ├── __pycache__
│   │   │   └── do_fi_to_es_body.cpython-36.pyc
│   │   └── tests
│   │       ├── A_exportas.tcl -> input_filter_one_line.tcl
│   │       ├── B_exportas.tcl
│   │       ├── input_directive.tcl
│   │       ├── input_filter_all_fields.tcl
│   │       ├── input_filter_bool.tcl
│   │       ├── input_filter_multi_line.tcl
│   │       ├── input_filter_one_line.tcl
│   │       └── tmp.json
│   ├── __pycache__
│   │   └── eda_status_filter_util.cpython-36.pyc
│   └── tests
│       ├── 0in_export_status.tcl
│       └── cdc_filter_directives.tcl
├── cdc_vio_sum_dash
│   ├── filter_switch.py
│   └── wildcard_expand.py
├── cfi_label
│   ├── labeling
│   └── playground
├── es_utils
│   ├── es_connect.py
│   ├── es_maintain.py
│   ├── idx_cdc_cons_history.json
│   ├── idx_cdc_vio.json
│   ├── idx_cdc_vio_set.json
│   ├── json2es.py
│   ├── Makefile
│   └── __pycache__
│       ├── es_connect.cpython-36.pyc
│       └── json2es.cpython-36.pyc
├── gerrit
│   └── apis
│       └── proc_access
│           ├── admin_cred_8090.txt
│           ├── clean_acc_ref.py
│           ├── clone_cmds.txt
│           └── test_repository
├── ip_admin
│   └── pjt2cfi
│       ├── 1650
│       └── david
│           └── xls_parser.py
├── knowbase_es
│   ├── 3ms
│   ├── svn
│   ├── web
│   └── wiki
│       └── tests
│           └── wiki.json
├── next_commit.org
├── README.md
├── requirements.txt
└── wst
    ├── testset_es
    │   ├── es_maintain.py
    │   ├── idx_wst_testset.json
    │   └── try_idx_wst_testset.txt
    └── testset_preproc
        └── wst_qa2md.py
```

