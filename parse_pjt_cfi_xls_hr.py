# coding:UTF-8

"""
xls里面的也是tree,只不过“判断是否有孩子”这个动作需要用openpyxl来实现,所以代码可以划分成3部分:怎么用openpyxl来表示tree,怎么遍历,怎么构造用dict表示的tree
"""

import pdb
from pprint import pprint

from openpyxl import load_workbook
from copy import deepcopy

import logging

# 创建并设置logger
logger = logging.getLogger('my_logger')
logger.setLevel(logging.DEBUG)  # 设置日志级别为DEBUG

# 创建一个formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s - %(filename)s:%(lineno)d')

# 创建一个handler，例如控制台输出
console_handler = logging.StreamHandler()
console_handler.setFormatter(formatter)

# 如果你想要日志输出到文件，可以创建一个FileHandler
# file_handler = logging.FileHandler('my_log.log')
# file_handler.setFormatter(formatter)

# 将handler添加到logger
logger.addHandler(console_handler)
# 如果你创建了FileHandler，也添加到logger
# logger.addHandler(file_handler)


NODE_TMPLATE = {
    "node_name": "SCOPE",
    "owner": {
        "ROLE": "OWNER"
    },
    "children": [
    ]
}


class XlsTreeNode:
    scope = None
    role = None
    owner = None
    children = None
    # 方便定位,打印xls上对应的位置
    col_idx = None
    row_idx = None

    def __init__(self, scope, role, owner, children=[]):
        self.scope = scope
        self.role = role
        self.owner = owner
        self.children = children

    def __repr__(self):
        return f"SCOPE:{self.scope},ROLE:{self.role},OWNER:{self.owner}"

    def to_dict(self):
        return {"node_name": self.scope,
                "owner": {self.role if self.role else "责任人": self.owner},
                # "children": deepcopy(self.children)  TODO BUG 如果不deepcopy会有嵌套list!
                "children": deepcopy(self.children)
                }


class XlsTree:
    xls_path = None
    xls_sheet = None
    wb = None
    ws = None

    # 在xls上移动的指针
    cur_col_idx = None
    cur_row_idx = None

    tree = None

    def __init__(self, path, sheet):
        self.xls_path = path
        self.xls_sheet = sheet
        self.wb = load_workbook(self.xls_path)
        self.ws = self.wb[self.xls_sheet]
        self.tree = {}

    def xls_move_right(self, ci:str, ri:int, delta:int):
        # 传入ci ri: 用于临时移动
        # 如果传入的是self.cur_col_idx,是在正式移动
        # CHECKPOINT 如果要扩展支持不同格式的xls存储的树结构,编辑此处的移动规则
        for i in range(delta):
            ci = get_next_letter(ci)
        return ci, ri

    def xls_move_down(self, ci:str, ri:int, delta:int):
        for i in range(delta):
            ri += delta
        return ci, ri

    def xls_is_valid_node(self, ci:str, ri:int):
        scope_col_idx = ci
        role_col_idx = get_next_letter(scope_col_idx)
        owner_col_idx = get_next_letter(role_col_idx)
        # pdb.set_trace()
        return get_cell_value(self.ws, scope_col_idx, ri) != None and \
               get_cell_value(self.ws, owner_col_idx, ri) != None and \
               get_cell_value(self.ws, scope_col_idx, ri) != '/' and \
               get_cell_value(self.ws, owner_col_idx, ri) != '/'

    def xls_area_to_node(self, ci, ri):
        scope_ci = ci
        role_ci, _ = self.xls_move_right(ci, ri, 1)
        owner_ci, _ = self.xls_move_right(ci, ri, 2)
        scope = get_cell_value(self.ws, scope_ci, ri)
        role = get_cell_value(self.ws, role_ci, ri)
        owner = get_cell_value(self.ws, owner_ci, ri)
        return XlsTreeNode(scope, role, owner)

    def xls_has_child(self):
        tmp_ci = self.cur_col_idx
        tmp_ri = self.cur_row_idx
        tmp_ci, tmp_ri = self.xls_move_down(tmp_ci, tmp_ri, 1)
        next_row_valid = self.xls_is_valid_node(tmp_ci, tmp_ri)
        tmp_ci, tmp_ri = self.xls_move_right(tmp_ci, tmp_ri, 3)
        child_valid = self.xls_is_valid_node(tmp_ci, tmp_ri)
        # pdb.set_trace()
        return not next_row_valid and child_valid, tmp_ci, tmp_ri

    def xls_has_sibling(self, cur_ci, cur_ri):
        """
        cur_ci: 当前子树根节点的col_idx
        cur_ri: 当前子树根节点的row_idx
        self.cur_col_idx 应当指向当前子树的最右下的节点的col_idx
        self.cur_row_idx 应当指向当前子树的最右下的节点的row_idx
        """
        tmp_ci = cur_ci
        tmp_ri = self.cur_row_idx + 1
        return self.xls_is_valid_node(tmp_ci, tmp_ri)

    def dfs(self, root_ci, root_ri, parent):
        print(root_ci, root_ri)
        assert self.xls_is_valid_node(root_ci, root_ri), f"当前位置不符合填写规范: {root_ci}{root_ri}"
        cur_node_dict = self.xls_area_to_node(root_ci, root_ri).to_dict()
        parent.append(cur_node_dict)
        self.cur_col_idx = root_ci
        self.cur_row_idx = root_ri

        has_child, child_ci, child_ri = self.xls_has_child()
        if has_child:
            self.dfs(child_ci, child_ri, cur_node_dict["children"])

        while self.xls_has_sibling(root_ci, root_ri):
            sib_ci = root_ci
            sib_ri = self.cur_row_idx + 1
            self.dfs(sib_ci, sib_ri, parent)

    def dump(self):
        # to json/yaml ...
        pass


def get_node(ws, cur_col_idx, cur_row_idx):
    ci_scope = cur_col_idx
    ci_role = get_next_letter(ci_scope)
    ci_owner = get_next_letter(ci_role)

    assert ws[f"{ci_scope}2"].value == "Scope", "现在处理的三元组的第一列必须是Scope"
    assert ws[f"{ci_role}2"].value == "Role", "现在处理的三元组的第二列必须是Role"
    assert ws[f"{ci_owner}2"].value == "Owner", "现在处理的三元组的第三列必须是Owner"

    scope = ws[f"{ci_scope}{cur_row_idx}"].value.strip()
    role = ws[f"{ci_role}{cur_row_idx}"].value.strip()
    owner = ws[f"{ci_owner}{cur_row_idx}"].value.strip()
    if role == '':
        role = "责任人"

    node = deepcopy(NODE_TMPLATE)
    node["node_name"] = scope
    node["owner"][role] = owner

    return node


def need_new_hr_tree(ws, cur_col_idx, cur_row_idx):
    if cur_col_idx == 'A' and ws[f"{cur_col_idx}{cur_row_idx}"].value.strip() != '':
        return True


def one_tree_dfs():
    pass


def get_cell_value(ws, ci, ri):
    if ws[f"{ci}{ri}"].value is None:
        return
    return ws[f"{ci}{ri}"].value.strip()


def get_next_letter(letter):
    # 检查是否是字母
    if letter.isalpha():
        # 计算下一个字母
        return chr(ord(letter) + 1)
    else:
        return None  # 或者抛出一个异常，或者返回一个错误信息


def is_valid_3_col(ws, cur_col_idx, cur_row_idx):
    scope_col_idx = cur_col_idx
    role_col_idx = get_next_letter(scope_col_idx)
    owner_col_idx = get_next_letter(role_col_idx)
    return get_cell_value(ws, scope_col_idx, cur_row_idx) != '' and \
           get_cell_value(ws, owner_col_idx, cur_row_idx) != ''


def should_move_down(ws, cur_col_idx, cur_row_idx):
    scope_col_idx = cur_col_idx
    role_col_idx = get_next_letter(scope_col_idx)
    owner_col_idx = get_next_letter(role_col_idx)
    next_scope_col_idx = get_next_letter(owner_col_idx)

    if ws[f"{scope_col_idx}{cur_row_idx}"].value.strip() != '':
        if ws[f"{owner_col_idx}{cur_row_idx}"].value.strip() != '':
            if ws[f"{next_scope_col_idx}{cur_row_idx}"].value.strip() == '/':
                return True

    return False


def is_eor(cur_col_idx):
    """
    End of row.
    """
    return cur_col_idx >= 'Y'

def should_move_right(ws, cur_col_idx, cur_row_idx):
    """
    是否需要向右移动3列
    """
    scope_col_idx = cur_col_idx
    role_col_idx = get_next_letter(scope_col_idx)
    owner_col_idx = get_next_letter(role_col_idx)
    next_scope_col_idx = get_next_letter(owner_col_idx)

    if get_cell_value(ws, scope_col_idx, cur_row_idx) == '' and \
       get_cell_value(ws, role_col_idx, cur_row_idx) == '' and \
       get_cell_value(ws, owner_col_idx, cur_row_idx) == '' and \
       get_cell_value(ws, next_scope_col_idx, cur_row_idx) != '/':
        return True    # 跳过空的三元组
    elif is_eor(cur_col_idx):
        return False
    return False


def is_eof(ws, cur_row_idx):
    """
    End of file/sheet
    """
    not_eof = False
    for i in range(10):
        ri = cur_row_idx + i
        ci = 'A'
        for j in range(30):
            if get_cell_value(ws, ci, ri) != '':
                not_eof = True
                break
            ci = get_next_letter(ci)

        if not_eof is True:
            break

    return not not_eof


def goto_next_valid(ws, cur_ci, cur_ri):
    if should_move_right(ws, cur_ci, cur_ri):
        cur_ci = get_next_letter(get_next_letter(get_next_letter(cur_ci)))
    elif should_move_down(ws, cur_ci, cur_ri):
        cur_ci = 'A'
        cur_ri += 1

    while not is_valid_3_col(ws, cur_ci, cur_ri):
        if should_move_right(ws, cur_ci, cur_ri):
            cur_ci = get_next_letter(get_next_letter(get_next_letter(cur_ci)))
        elif should_move_down(ws, cur_ci, cur_ri):
            cur_ci = 'A'
            cur_ri += 1
    return cur_ci, cur_ri


class ParentNode:
    col_idx = None
    row_idx = None
    value = None

    def __init__(self, ws, ci, ri):
        self.col_idx = ci
        self.row_idx = ri
        self.value = get_cell_value(ws, ci, ri)


class ParentStack:
    parents = None    # parent的序列,每个元素是历史parent的位置信息,遍历时的辅助信息,遍历完以后应该存着所有的A列最后一个有效节点
    parent = None    # 指向当前的parent

    def __init__(self):
        self.parents = []

    def update_tail(self, ws, cur_ci, cur_ri, cur_node):
        if len(self.parents) == 0:
            self.push(ParentNode(ws, cur_ci, cur_ri))
        elif self.parents[-1].col_idx == cur_ci:    # last parent 和当前node是平级cell,直接替换
            """
            ___LST
            ___CUR <-- new parent
            """
            self.pop()
            self.push(ParentNode(ws, cur_ci, cur_ri))
        elif self.parents[-1].col_idx == cur_ci and self.parents[-1].row_idx < cur_ri:
            """
            LST
            ___CUR <-- new parent
            """
            self.push(ParentNode(ws, cur_ci, cur_ri))
        elif self.parents[-1].col_idx > cur_ci and self.parents[-1].row_idx < cur_ri:
            """
            ___LST
            CUR <-- new parent
            """
            self.pop()
            self.push(ParentNode(ws, cur_ci, cur_ri))

        self.parent = cur_node

    def push(self, node):
        self.parents.append(node)

    def pop(self):
        self.parents.pop()


def iter_xls(ws):
    cur_col_idx = 'A'
    cur_row_idx = 3

    to_ret = []
    parent_stack = ParentStack()

    while not is_eof(ws, cur_row_idx):

        if is_valid_3_col(ws, cur_col_idx, cur_row_idx):
            new_node = get_node(ws, cur_col_idx, cur_row_idx)
            if need_new_hr_tree(ws, cur_col_idx, cur_row_idx):
                to_ret.append(new_node)
                parent_stack.parent = new_node
                parent_stack.update_tail(ws, cur_col_idx, cur_row_idx, new_node)
            else:    # 需要把当前node添加到parent里
                parent_stack.parent["children"].append(new_node)
                parent_stack.update_tail(ws, cur_col_idx, cur_row_idx, new_node)
            parent_stack.parent["children"].append(new_node)

            print(cur_col_idx, cur_row_idx)
            cur_col_idx, cur_row_idx = goto_next_valid(ws, cur_col_idx, cur_row_idx)
            print(cur_col_idx, cur_row_idx)
        print(cur_col_idx, cur_row_idx)

    return to_ret


if __name__ == "__main__":
    hr_tree_list = [{}]
    iter_xls(ws)


if __name__ == "__main__":
    xls_tree = XlsTree("/path/to/xlsx", "SheetName")
    to_dump = []
    xls_tree.dfs('A', 3, to_dump)
    # pprint(to_dump)
    with open("", 'w') as fout:
        json.dump(to_dump, fout, indent=4, ensure_ascii=False)

