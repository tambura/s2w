```mermaid
---
title: 模块仓库 hdl/xx_
---
%%{init: { 'logLevel': 'debug', 'theme': 'base', 'gitGraph': {'showBranches': true, 'showCommitLabel':true,'mainBranchName': 'master'}} }%%
gitGraph
    commit id: "..." tag: "B500"
    branch sync2hdl_p
    branch sync2hdl_f

    checkout master
    
    commit id: "25/01/09:pre_rls"
    commit id: "25/01/21:fnl_rls"
    commit id: "25/01/13" tag: "B510开工"

    checkout sync2hdl_f
    commit id: "25/01/24"

    checkout sync2hdl_p
    commit id: "25/01/25"
```

```mermaid
---
title: 聚合仓库 hdl_p/xx
---
%%{init: { 'logLevel': 'debug', 'theme': 'base', 'gitGraph': {'showBranches': true, 'showCommitLabel':true,'mainBranchName': 'master'}} }%%
gitGraph
    commit id: "2004-10-01"
    branch bes
    branch fpga

    checkout master
    commit id: "2004-12-04"

    checkout bes
    commit id: "2024-12-20"

    checkout fpga
    commit id: "24/12/26" tag: "B500"

    checkout bes
    commit id: "25/01/26" tag: "B510pre"

    checkout master
    commit id: "25/01/25" tag: "B510"

```

```mermaid
---
title: 聚合仓库 hdl_f/xx
---
%%{init: { 'logLevel': 'debug', 'theme': 'base', 'gitGraph': {'showBranches': true, 'showCommitLabel':true,'mainBranchName': 'master'}} }%%
gitGraph
    commit id: "2023-10-01"
    branch bes
    branch fpga
    checkout master
    commit id: "2023-10-04"

    checkout bes
    commit id: "24/12/28"

    checkout fpga
    commit id: "24/12/26"

    checkout master
    commit id: "25/01/24" tag: "B510"
```
