# UT验证环境的分支建议

## 现状预估

目测大家还是习惯trunk-based的分支模式，即“本地只有一个分支，只服务于hdl/xx最新的RTL”

## 要解决的问题

- UT主要验证hdl/xx里面的RTL，这里比hdl_xx[master]领先一些
- 项目后半段会出现多die节奏不一，验证环境可能需要分别适配多个die的行为

## 术语约定

为了避免描述有歧义，先约定一组描述方法，后文会经常见到。

- RTL::hdl/xx[rls] 资源组的模块RTL仓库，每个模块有自己的仓库
- RTL::hdl_yy[master] 项目组的聚合RTL仓库，各模块的RTL放在一个仓库hdl_yy中
- ENV::blk_xx[feature] 这里是为了验证 RTL::hdl/xx[rls]
- ENV::blk_xx[rls] 这里是为了验证 RTL::hdl_yy[master]
- ENV::blk_xx[feature_die_yy] 这里是为了验证 RTL::hdl_yy[master] 上die特有的行为

## 推荐做法

### 高频工作分支为 `ENV::blk_xx[feature]`

- 这里验证的是 `RTL::hdl/xx[rls]`
- 如果当前验证环境只有master分支，可以岔出一个feature分支
  - 观念上要调换主次，虽然名字变成“feature分支”，但是这里是UT高频工作的分支

### 最终交付分支为 `ENV::blk_xx[rls]`

- 这里验证的是 `RTL::hdl_yy[master]`
- 如果当前验证环境只有master分支，master分支适配 `RTL::hdl_yy[master]` 后可不再频繁更新
  + 更新场景一：下一个B版本的 hdl_yy[master] 发布后，需要更新
  + 更新场景二：设计在`RTL::hdl_yy[master]`上做了bugfix，需要更新
- `[rls]` 中rls的具体名字推荐格式：
  + 对于OneTrack的模块：使用`rls_1952`
  + 对于非OneTrack的模块：使用`master`也可
- 对于交付多DIE的模块，这个分支要能支持验证各DIE上公共的行为

### 应对DIE特有行为的分支为 `ENV::blk_xx[feature_die_yy]`

> 对于只交一个DIE的模块可忽略这里

- 这里验证的是 `RTL::hdl_yy[master]`，但是主要目的是验证yy DIE特有的行为
- 在项目后期某个时间开始，`RTL::hdl_yy[master]` 不再从 `RTL::hdl/xx[rls]` 同步代码过来，上面这个分支可作为交付yy DIE的主验证环境