import json
import sys


MD_TEMPLATE = """
# {n}

## Q

### Q1 {q1_short}...
{q1}

## A

### A1

> URL: {ref_url_1}

{a1}

### A2

> URL: {ref_url_2}

{a2}

## 评论

### 张三@20240416
A1的URL应该为：xx协议yy章zz节。
A2的URL应该为：xx协议yy章zz节。

欢迎在此处追加评论，点击右上角的“编辑”，新起一行增加一个 `### <WHO><WHEN>` 简单写写你的理解或指定更正确的参考文档即可

---

"""

with open(sys.argv[1]) as fin:
    wst_qas = json.load(fin)


to_write = ""

for n, qa in enumerate(wst_qas):
    qa_md = MD_TEMPLATE.format(
            n=n,
            q1_short=qa["question"][0:20] if len(qa["question"]) > 20 else qa["question"],
            q1=qa["question"],
            ref_url_1=qa["output"][0]["reference"]["metadata"]["title"] + '/' + qa["output"][0]["reference"]["metadata"]["header"],
            a1=qa["output"][0]["response"],
            ref_url_2=qa["output"][1]["reference"]["metadata"]["title"] + '/' + qa["output"][1]["reference"]["metadata"]["header"],
            a2=qa["output"][1]["response"])
    to_write += qa_md

with open(sys.argv[2], 'w') as fout:
    fout.write(to_write)

