flt_key_to_stt_key = None
stt_key_to_flt_key = None

"""
POST idx_cdc_vio_detail/_search
{
  "size": 0,
  "aggs": {
    "unique_values": {
      "terms": {
        "field": "TAG.scheme.keyword"
      }
    }
  }
}
"""
# 在RED实际在用的值有：
"""
no_sync
multi_bits
async_reset
multi_sync_mux_select
reconvergence
dmux
combo_logic
partial_dmux
fanin_different_clks
two_dff
"""
# 其他地方见到过的，可能会用的：
"""
and_gate_dnux
async_reset_no_sync
blackbox
blackbox_sync
bus_custom_sync
bus_custom_sync_mismatch
bus_custom_sync_with_crossing
bus_custom_sync_with_crossing_misnatch
bus_data_latch
bus_dff_sync_combo_clk
bus_dff_sync_gated_clk
bus_dff_sync_with_enable
bus_four_latch
bus_port_sync
bus_sync_gray
bus_shift_reg
bus_two_dff
bus_two_dff_phase
clock_no_sync
convergence_clockgate_redundant
custom_sync
custom_sync_mismatch
custom_sync_with_crossing
custom_sync_with_crossing_mismatch
data_latch
dff_sync_combo_clk
dff_sync_gated_clk
dff_sync_with_enable
dmux_no_hold
fifo
Fifo_menory
fifo_menory_ptr_mismatch
fifo_ptr_no_sync
forward_simple_dmux
four_latch
functional_false_path
handshake
hdm_sync
multi_clock_memory
mux_lock_dmux
pa_combo_logic
pa_fanin_different_clks
pa_iso_en_no_sync
pa_retention_restore
pa_retention_save
port_conbo_logic
port_sync
pulse_single_edge
pulse_sync
reconvergence_bus
reconvergence_gray
reconvergence_mixed
redundant
reg_enable_no_sync
series_redundant
shift_reg
simple_dmux
single_source_reconvergence
sync_enable
two_dff_phase
"""


"""
# 带空格的违例类型
POST idx_cdc_vio_detail/_search
{
  "size": 0,
  "aggs": {
    "unique_values": {
      "terms": {
        "field": "TAG.scheme_name.keyword"
      }
    }
  }
}
"""
"""
"Single-bit signal does not have proper synchronizer"
"Multiple-bit signal across clock domain boundary"
"Asynchronous reset synchronization"
"Combinational logic before synchronizer"
"Fanin logic from multiple clock domains"
"Multiple-bit signal synchronized by DFF synchronizer": "???",    # TODO
"Single Source Reconvergence of synchronizers",    # TODO ???
"Single-bit data latch signal across clock domain boundary",    # TODO ???,
"FIFO synchronization",    # TODO ???
"""



# KEY 是 filter/status 中会出现的字段
# VALUE 是 ES 中使用的字段
#   - 最开始ES中用的字段是从文本rpt中提取到的关键词
#   - 现在导入ES时用了0in的tcl接口来取
# 由于不同版本的0in可能会用不同的描述，可能会造成一些困惑，需要慢慢调试
cdc_type_enum_to_scheme = {
    "Async Reset No Sync": "async_reset_no_sync",
    "Asynchronous reset does not have proper synchronization": "async_reset_no_sync",
    "Asynchronous reset synchronization":"async_reset",
    "Combo Logic": "combo_logic",
    "Combinational logic before synchronizer": "combo_logic",
    "DFF Gated Sync": "dff_sync_gated_clk",
    "DMUX": "dmux",
    "DMUX synchronization": "dmux",
    "Fanin logic from different clocks": "fanin_different_clks",
    "Fanin Logic From Different Clocks": "fanin_different_clks",
    "Fanin logic from multiple clock domains": "fanin_different_clks",
    "Reconvergence": "reconvergence",
    "Missing Synchronizer": "no_sync",
    "Multiple Bits": "multi_bits",
    "Multiple-bit signal across clock domain boundary": "ulti_bits",
    "Multiple-bit signal synchronized by Custom CDC scheme": "bus_custom_sync",
    "Multiple-bit signal synchronized by DFF synchronizer": "???",    # TODO
    "Multiple Synchronizers at MUX Select": "multi_sync_mux_select",
    "Mux select fanin contains more than one synchronizer": "multi_sync_mux_select",
    "partial DMUX": "partial_dmux",    # ???
    "Partial DMUX": "partial_dmux",
    "Reconvergence of synchronizers": "single_source_reconvergence",
    "Redundant synchronization": "redundant",
    "Redundant": "redundant",
    "Shift Reg": "shift_reg",
    "Shift-register synchronization": "shift_reg",
    "Single-bit signal does not have proper synchronizer": "no_sync",
    "Single-bit signal synchronized by Custom CDC scheme": "custom_sync",
    "Single-bit signal synchronized by DFF synchronizer": "???",    # TODO
    "Simple DMUX synchronization" :"simple_dmux",
    "Two DFF Synchronizer": "two_dff",
}


# 左边是analysis中使用的key
## filter中使用的key：
## status中使用的key：
# cdc_type_enum_to_scheme = {
#     "Missing Synchronizer": "no_sync",
#     # TODO
# }

# kql_key 是点连接的root到leaf的一串key
flt_key_to_kql_key = {
    "protocal_chkr_name": "skip",
    "rx_filename": "skip",
    "rx_name": "RX.signal.keyword",
    "diverge_node": "skip",
    "reviewers": "skip",
    "tx_filename": "skip",
    "custom_sync_module": "skip",
    "tx_name": "TX.signal.keyword",
    "fx_chkr_name": "skip",
    "rx_clock": "RX.clock.keyword",
    "include_exclude": "skip",
    "cdc_fx_result": "skip",
    "cdc_mode": "skip",
    "cdc_static_result": "cdc_static_result",
    "cdc_type_enum": "TAG.scheme.keyword",
    "tx_clock": "TX.clock.keyword",
    "rx_module": "RX.module.keyword",
    "cdc_sim_result": "skip",
    "tx_module": "TX.module.keyword",
    "gdir_id": "skip",
    "owner": "skip",
    "chk_cdcid": "TAG.cdc_id.keyword",
    "cdc_base_type": "skip",
    "comments": "skip",
    "thrupoints": "skip",
    "_ziEnableFilter": "skip",
    "protocol_chkr_name": "skip"
}


# kql_key 是点连接的root到leaf的一串key
stt_key_to_kql_key = {
    "scheme": "TAG.scheme",
    "from": "TX.signal.keyword",
    "to": "RX.signal.keyword",
    "tx_clock": "TX.clock.keyword",
    "rx_clock": "RX.clock.keyword",
    "status": "TAG.status",
    "message": "TAG.message",
    "owner": "TAG.owner",
    "reviewers": "TAG.reviewers",
    "reviewer": "TAG.reviewers",
}


# 两个作用
# 一个是把不同名称、相同含义的key映射到ES json的某个具体的key
# 一个是把暂时忽略的字段忽略skip
# 注意：不是点连接的root到leaf的一串key
# 0in 上 fixed 的item不会被导出
stt_key_to_json_key = {
    "scheme": "skip",
    "from": "from",
    "to": "to",
    "tx_clock": "tx_clock",
    "rx_clock": "rx_clock",
    "status": "status",
    "message": "message",
    "owner": "owner",
    "reviewers": "reviewers",
    "reviewer": "TAG.reviewers",
}


# 两个作用
# 一个是把不同名称、相同含义的key映射到ES json的某个具体的key
# 一个是把暂时忽略的字段忽略skip
# 注意：不是点连接的root到leaf的一串key
flt_key_to_json_key = {
    "protocol_chkr_name": "skip",
    "rx_filename": "skip",
    "rx_name": "to",
    "diverge_node": "skip",
    "reviewers": "reviewers",
    "tx_filename": "skip",
    "custom_sync_module": "skip",
    "tx_name": "from",
    "fx_chkr_name": "skip",
    "rx_clock": "rx_clock",
    "include_exclude": "skip",    # only 1 will be converted.
    "cdc_fx_result": "skip",
    "cdc_mode": "skip",
    "cdc_static_result": "TAG.Severity.keyword",    # TAG.Severity.keyword
    "cdc_type_enum": "TAG.scheme.keyword",    # TAG.schema.keyword
    "tx_clock": "tx_clock",
    "rx_module": "skip",
    "cdc_sim_result": "skip",
    "tx_module": "skip",
    "gdir_id": "skip",
    "owner": "owner",
    "chk_cdcid": "skip",    # Filter always for multi violations. So skip.
    "cdc_base_type": "skip",
    "comments": "message",
    "thru_points": "skip",
    "_ziEnableFilter": "skip",
}
