# -*- coding:UTF-8 -*-
# status/filter are in cfg repo.
# Push will trigger a es indexing.

import pdb
from pprint import pprint
from copy import deepcopy

import os, sys
THIS_DIR = os.path.dirname(os.path.realpath(__file__))

import re
import json
import subprocess

sys.path.append(os.path.join(THIS_DIR, "../"))
from ana_file_util import ana_file_to_es_doc

sys.path.append(os.path.join(THIS_DIR, "../../../es_utils/"))
from es_connect import get_es
from json2es import bulk_index

import logging
logging.basicConfig(level=logging.ERROR,
                    format='%(filename)s:%(lineno)d - %(message)s')
logger = logging.getLogger(__name__)

sys.path.append(os.path.join(THIS_DIR, "../prepare"))
from ana_to_es_doc import add_tag_for_filter_file

from build_ana2vio_ubq import ana_to_updatebyquery_body


def dicts2json(stt_fltlter_dicts, json_path):
    with open(json_path, 'w') as fout:
        json.dump(stt_fltlter_dicts, fout, indent=2)


def relative_path_to_abs(cfg_path, stt_flt_path_relative_cfg):
    stt_flt_path_relative_cfg = stt_flt_path_relative_cfg.strip()
    stt_flt_path = os.path.join(cfg_path, stt_flt_path_relative_cfg)
    stt_flt_realpath = os.path.realpath(stt_flt_path)
    return stt_flt_realpath

def cfg_stt_flt_files_to_es_ana_dtl():
    with open(args.stt_flt_list_path) as fin:
        stt_flt_path_list = fin.readlines()

    all_stt_flt_list = list()
    for stt_flt_path_relative_cfg in stt_flt_path_list:
        stt_flt_list = ana_file_to_es_doc(relative_path_to_abs(args.cfg_path, stt_flt_path_relative_cfg))
        to_es_list = add_tag_for_filter_file(args, stt_flt_list, stt_flt_path_relative_cfg)
        # pprint(to_es_list)
        all_stt_flt_list.extend(to_es_list)

    if args.debug:
        dicts2json(all_stt_flt_list, args.json)
    else:
        es = get_es(args.env)
        bulk_index(es, all_stt_flt_list, args.batch_size, args.index)


def cfg_stt_flt_files_to_es_vio_dtl():
    # cfg中有有效更新的时候触发
    # 这个文件列表的内容：仅列出本次更新涉及的stt_flt
    # 意味着，只更新被pattern命中的exc_bitmap，vio_dtl中的bitmap无法展开成一个矩阵
    # 另外一种方案：暂不采用：exc_bitmap 沿时间展开后，是一个全量矩阵，横轴表示cfg的每次提交时间，
    with open(args.stt_flt_list_path) as fin:
        stt_flt_path_list = fin.readlines()

    es = get_es(args.env)

    for stt_flt_path_relative_cfg in stt_flt_path_list:
        ubq_list = ana_to_updatebyquery_body(args, relative_path_to_abs(args.cfg_path, stt_flt_path_relative_cfg))
        for ubq_one_entry in ubq_list:
            if args.debug:
                logger.debug(json.dumps(ubq_one_entry))
                dicts2json(ubq_one_entry, args.json)
                tmp_list = ubq_one_entry["query"]["bool"]["should"]
                response = es.count(index=args.index, body={"query": {"bool": {"should": tmp_list}}})
                print(response)
            else:
                response = es.update_by_query(
                    index=args.index,  # 指定索引
                    body={
                        "script": ubq_one_entry["script"],
                        "query": ubq_one_entry["query"]
                    },
                    wait_for_completion=False
                    # conflicts="proceed"  # 处理版本冲突的策略，可以是 "abort" 或 "proceed"
                )
                logger.info(f"Apply analysis file {stt_flt_path_relative_cfg} to violation got the respoons:\n\t{response}")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Put docs to es')
    parser.add_argument('--action', type=str)

    parser.add_argument('--env', type=str)
    parser.add_argument('--index', type=str)
    parser.add_argument('--bulk', action='store_true')
    parser.add_argument('--batch_size', type=int)
    # Meta
    parser.add_argument('--project', type=str)
    parser.add_argument('--top', type=str)
    # Input path
    parser.add_argument('--cfg_path', type=str)
    parser.add_argument('--stt_flt_list_path', type=str, help='status/filter file path (relative to cfg_path) list')
    # To clear
    parser.add_argument('--clear', action='store_true')
    # To debug
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--json', type=str)
    args = parser.parse_args()
 

    if args.action == "to_ana_dtl":
        # 把stt_flt搞进 idx_cdc_ana_dtl
        #   切换到这个模式后，就不要在生成测试用例时写入ana_dtl了
        cfg_stt_flt_files_to_es_ana_dtl()
    elif args.action == "to_vio_dtl":
        # zzzTODO 把stt_flt搞进 idx_cdc_vio_dtl
        cfg_stt_flt_files_to_es_vio_dtl()
