# -*- coding:UTF-8 -*-
import sys, os
import re
from pprint import pprint
import pdb
THIS_DIR = os.path.dirname(os.path.realpath(__file__))

sys.path.append(os.path.join(THIS_DIR, "../../"))
from common_map import stt_key_to_json_key
from common_map import flt_key_to_json_key
from common_map import stt_key_to_kql_key
from common_map import flt_key_to_kql_key
from common_map import cdc_type_enum_to_scheme
from logger import logger


# Common preprocess [[
def clear_comment(lines):
    return_list = list()
    for line in lines:
        if re.match(r"\s*#", line):
            continue
        elif re.match(r"^\s*$", line):
            continue
        else:
            return_list.append(line)

    return return_list
# Common preprocess ]]

# Filter prepcoess [[

def should_split_filters(lines):
    lines_without_comment = clear_comment(lines)
    if len(lines_without_comment) == 1:
        return True
    else:
        return False


def is_comment_line(line):
    return re.match(r'^\s*#', line)


def is_split_by_user(lines):
    """
    用户可能自己把filter文件分行了
    """
    if len(lines) > 1:
        idx = 0
        while is_comment_line(lines[idx]):
            idx += 1
        return re.match(r'.*\\\s*$', lines[idx])


def clean_lines(lines):
    """
    把filter的有效负载留下
    """
    def should_skip(line):
        if is_comment_line(line):
            return True
        if re.match(r'.*list\s+\\\s*$', line):
            # 只处理 set list [ \ 这种后面不跟有效负载的行
            return True
        if re.match(r'^\s*\]\s*$', line):
            # 最后一行会有一个 ]
            return True
        if re.match(r'^\s*\\\s*$', line):
            # \ 拼接了一个空entry
            return True
        if re.match(r'^\s*$', line):
            # 纯空行
            return True

    def clean_line(line):
        return re.sub('\s*\\\s*', '', line)

    to_ret = []
    for line in lines:
        line = line.strip()
        # pdb.set_trace()
        if should_skip(line):
            continue
        to_ret.append(clean_line(line))

    return to_ret


def split_one_line_filter(line):
    # pdb.set_trace()
    line = re.sub(r'} {', "} \n{", line)
    line = re.sub(r'set\s+filters\s+\[list\s+{', "{", line)
    line = re.sub(r'\s+\]$', "", line)
    return line.strip().split('\n')


def preproc_filter_lines(filter_lines):
    """
    Preprocess filter lines
    """
    if should_split_filters(filter_lines):
        lines = split_one_line_filter(filter_lines[0])
    elif is_split_by_user(filter_lines):
        lines = clean_lines(filter_lines)
    else:
        lines = filter_lines
    return lines

# Filter prepcoess ]]


def get_ana_file_lines(file_path, do_or_fi):
    with open(file_path) as fin:
        lines = fin.readlines()
    if do_or_fi == "filter":
        lines = preproc_filter_lines(lines)
    elif do_or_fi == "directive":
        # zzzTODO lines = preproc_status_lines(lines)
        pass
    else:
        raise Exception(f"Analysis file: {file_path} not support!")

    return lines


def status_or_filter_entry(one_line):
    mode = None
    if "cdc report item" in one_line:
        mode = "directive"
    else:
        mode = "filter"

    return mode


def status_or_filter_file(file_path):
    with open(file_path) as fin:
        lines = fin.readlines()
    lines = clear_comment(lines)
    do_or_fi = status_or_filter_entry(lines[0])

    return do_or_fi


def find_non_empty_fields(one_line, do_or_fi):
    """
    For one filter that has multi fields.
    :return: k-v dict, do_or_filter
    """
    def del_quo(in_str):
        if in_str.startswith('{'):
            in_str = in_str[1:]
        if in_str.endswith('}'):
            in_str = in_str[:-1]
        return in_str

    # print(one_line)
    if do_or_fi == "directive":
        rf = re.findall(r"-(\w+)\s+(.+?)\s", one_line)
    else:
        # will fail at "key {with space inside}"
        # rf = re.findall(r"(\w+)\s+(.+?)\s", one_line)

        # group1 (\w+)\s+, group2 (\{(?:[^}]*)\}|[^{}\s]+)\s* 这里的意图：如果没遇到{}就空格停，如果遇到{则}停
        rf = re.findall(r"(\w+)\s+(\{(?:[^}]*)\}|[^{}\s]+)\s*", one_line)

    to_ret = {}
    for kv in rf:
        if kv[1] != "{}":
            key = del_quo(kv[0])
            val = del_quo(kv[1])
            to_ret.update({key: val})

    return to_ret


def ana_entry_to_es_doc(line, do_or_fi):
    ana_kv = find_non_empty_fields(line, do_or_fi)
    if "cdc_static_result" in ana_kv:
        if do_or_fi == "filter" and ana_kv["cdc_static_result"] == "Caution":
            logger.info(f"Skip line: {line} due to cdc_static_result == Caution")
            return None

    if do_or_fi == "filter" and ana_kv["include_exclude"] == '0':
        logger.info(f"Skip line: {line} due to include_exclude == 0")
        return None

    if do_or_fi == "filter":
        fields_map = flt_key_to_json_key
    elif do_or_fi == "directive":
        fields_map = stt_key_to_json_key

    return_item = {}
    for key, val in ana_kv.items():
        if fields_map[key] != "skip":
            if key == "cdc_type_enum":
                assert val in cdc_type_enum_to_scheme, f"Please update cdc_type_enum_to_scheme for '{val}'"
                return_item.update({fields_map[key]: cdc_type_enum_to_scheme[val]})
                continue

            return_item.update({fields_map[key]: val})

    return return_item


def ana_file_to_es_doc(file_path):
    """
    file_path: 用绝对路径
    return: 要返回到哪一层？
    """
    do_or_fi = status_or_filter_file(file_path)

    lines = get_ana_file_lines(file_path, do_or_fi)

    return_list = list()
    for line in lines:
        entry_kv = ana_entry_to_es_doc(line, do_or_fi)
        if entry_kv is None:
            continue
        return_list.append(entry_kv)

    return return_list
 

def ana_entry_to_es_kql(file_path):
    """
    kql: 主要是用 . 拼接的路径
    """
    pass


def ana_file_to_es_kql(file_path):
    """
    kql: 主要是用 . 拼接的路径
    """
    pass


def ana_entry_to_es_qb(line, do_or_fi):
    """
    es query body
    """
    ana_kv = find_non_empty_fields(line, do_or_fi)
    if "cdc_static_result" in ana_kv:
        if do_or_fi == "filter" and ana_kv["cdc_static_result"] == "Caution":
            logger.info(f"Skip line: {line} due to cdc_static_result == Caution")
            return None

    if do_or_fi == "filter":
        assert "include_exclude" in ana_kv, f"{line} do NOT have include_exclude"
        if ana_kv["include_exclude"] == '0':
            logger.info(f"Skip line: {line} due to include_exclude == 0")
            return None

    if do_or_fi == "filter":
        fields_map = flt_key_to_kql_key
    elif do_or_fi == "directive":
        fields_map = stt_key_to_kql_key
    else:
        exit(1)

    return_item = {}
    for key, val in ana_kv.items():
        # TODO 当前的index里面没有这个字段，临时跳过
        if key == "cdc_static_result":
            continue

        if fields_map[key] != "skip":
            if key == "cdc_type_enum":
                assert val in cdc_type_enum_to_scheme, f"Please update cdc_type_enum_to_scheme for '{val}'"
                return_item.update({fields_map[key]: cdc_type_enum_to_scheme[val]})
                continue

            return_item.update({fields_map[key]: val})

    return return_item


def ana_file_to_es_qb(file_path):
    """
    es query body
    """
    logger.info(f"\n\n***** 当前的analysis文件为：{file_path}\n")
    do_or_fi = status_or_filter_file(file_path)
    lines = get_ana_file_lines(file_path, do_or_fi)
    return_list = list()
    for line in lines:
        entry_kv = ana_entry_to_es_qb(line, do_or_fi)
        if entry_kv is None:
            continue
        return_list.append(entry_kv)

    return return_list
