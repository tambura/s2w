from pprint import pprint

import os
import sys
import json

THIS_DIR = os.path.dirname(os.path.realpath(__file__))

sys.path.append(os.path.join(THIS_DIR, "../"))
from ana_file_util import ana_file_to_es_qb

sys.path.append(os.path.join(THIS_DIR, "../prepare"))
# from map_stt_flt_to_es_doc import fields_map

sys.path.append(os.path.join(THIS_DIR, "../"))
from query_body_util import a_entry_to_query_body

sys.path.append(os.path.join(THIS_DIR, "../../../es_utils/"))
from es_connect import get_es

sys.path.append(os.path.join(THIS_DIR, "../../../"))
from logger import logger


def check_each_ana_entry(file_path, es):
    """
    一个模块对应一个文件，一般会有多个filter entry
    """
    do_fi_list = ana_file_to_es_qb(file_path)
    for do_fi_dict in do_fi_list:
        q = a_entry_to_query_body(do_fi_dict)
        if q:
            # 这种写法的原因：使用该query后留下还没有被分析掉的违例
            rsp = es.count(index=args.index, body=q)
            # print(rsp)
            if rsp["count"] == 0:
                logger.warning(f"This entry could not match any violation:\n\t{json.dumps(q)}")
            else:
                logger.info(f"Matched {rsp['count']} violations:\n\t{json.dumps(q)}")
            # pprint(q)


if __name__ == "__main__":
    import argparse
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--env', type=str)
    arg_parser.add_argument('--index', type=str)
    arg_parser.add_argument('--ana_file', type=str)
    # arg_parser.add_argument('--unit', type=str, help="指定检查的粒度", choices=["entry", "file"])
    args = arg_parser.parse_args()

    es = get_es(args.env)
    check_each_ana_entry(args.ana_file, es)
