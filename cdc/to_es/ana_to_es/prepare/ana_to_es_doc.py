# -*- coding:UTF-8 -*-
"""
在仓库、文件、analysis_entry 的粒度上提取 status/filter 的信息，转换成 es 的 index doc
"""

import os
import re
import subprocess
from copy import deepcopy
from datetime import datetime


import logging
logging.basicConfig(level=logging.ERROR,
                    format='%(filename)s:%(lineno)d - %(message)s')
logger = logging.getLogger(__name__)


TMP_META = {
    # meta from args
    "project": "V_PJT_1",
    "top": "V_TOP_A",
    # meta from cfi
    "hdl_revision": "1ab2c3d-changeid-commit-id",
    "run_date": "2024-04-16",
    # meta from cfg_repo
    "cfg_revision": "changeid_1",
    "update_date": "2024-04-09",
    "module": "GIC->GIC",    # 这里的module只从status/filter的文件名中提取
    # 还有一种方式是从文件内容中提取，但是有点太复杂，后面可能会采用
    # meta from file contents
    "message": "why_waived",
    "owner": "a00123456",
    "reviewers": ["x00123456", "y00345678"],
    "scheme": "no_sync"
}


def get_meta_from_upstream_call(args):
    return args.project, args.top


def get_meta_from_cfi():
    """
    获取当前run窗口期间的hdl_revision和run_date
    """
    # if args.project is not None and args.top is not None:
    last_run = "2024-04-09"
    hdl_revision = "1ab2c3d-changeid-commit-id"
    run_date = "2024-04-16"

    return hdl_revision, run_date


def get_meta_from_cfg_repo(args, filename: str):
    """
    filename: 相对于cfg仓库的路径
    """
    def get_die_top(filename):
        # TODO die_top pattern as parameter
        die_top = re.findall(r'(<?=/))\w+_DIE_TOP(?=/))', filename)[0].strip()
        return die_top

    def get_latest_commit(repo_path, filename):
        cmd = "git log -1 %s" % filename
        stdout = subprocess.check_output(cmd, shell=True, cwd=repo_path)
        return stdout.decode().strip() 

    def get_update_date(cmt_log_stdout):
        date_str = re.findall(r'Date:\s+(.*)', cmt_log_stdout)[0].strip()
        cmt_date = datetime.strptime(date_str, "%a %b %d %H:%M:%S %Y %z")
        return cmt_date.strftime("%Y-%m-%d")

    def get_cfg_revision(cmt_log_stdout):
        cfg_revision = re.findall(r'commit\s+(.*)', cmt_log_stdout)[0].strip()
        return cfg_revision

    def get_owner(cmt_log_stdout):
        # 优先看message里是否有指定的OWNER，如果存在优先返回
        owner = re.findall(r'.*\[OWNER:(\w\d+)\]', cmt_log_stdout)[0].strip()
        if owner:
            return owner
        owner = re.findall(r'Author:\s+(.*)', cmt_log_stdout)[0].strip()
        if owner:
            return owner
        return

    die_top = get_die_top(filename)
    cmt_log_stdout = get_latest_commit(args.cfg_path, filename)
    cfg_revision = get_cfg_revision(cmt_log_stdout)
    logger.debug(cfg_revision)
    update_date = get_update_date(cmt_log_stdout)
    logger.debug(update_date)
    owner = get_owner(cmt_log_stdout)
    logger.debug(owner)

    return die_top, cfg_revision, update_date, owner


def get_owner_from_cfg_repo(args, filename: str):
    """
    filename: 相对于cfg仓库的路径
    """
    cfg_revision, update_date, owner = get_meta_from_cfg_repo(args, filename)
    return owner


def get_meta_from_file_name(filename: str):
    def get_module(filename):
        if filename.endswith("_export.filter"):
            rec = re.compile(r'(.*)_export.filter')
        elif filename.endswith("_export_as.filter"):
            rec = re.compile(r'(.*)_export_as.filter')
        elif filename.endswith("_export.status"):
            rec = re.compile(r'(.*)_export.status')
        else:
            logger.error(f"Can't get module from {filename}")
            return None

        module = re.findall(rec, filename)[0]
        if module is None:
            module = re.findall(rec, filename)[0]
        if module is None:
            logger.error("Can't get module from %s" % filename)
        return module

    return get_module(os.path.basename(filename))


def get_meta_from_entry(args, entry_dcit: dict, filename: str):
    if "owner" not in entry_dcit:
        owner = get_owner_from_cfg_repo(args, filename)
    else:
        owner = entry_dcit["owner"]

    if "message" not in entry_dcit:
        message = "EMPTY"
    else:
        message = entry_dcit["message"]

    if "reviewers" not in entry_dcit:
        reviewers = "EMPTY"
    else:
        reviewers = entry_dcit["reviewers"]

    if "scheme" not in entry_dcit:
        scheme = "EMPTY"
    else:
        scheme = entry_dcit["scheme"]

    return message, owner, reviewers, scheme


def add_tag_for_filter_file(args, in_dicts: list, stt_flt_path_relative_to_cfg: str):
    """
    Add tag meta to docs. Docs is all items of one status/filter file.
    in_dicts: list of dicts
    stt_flt_path_relative_to_cfg: str. file path of the status/filter.相对于cfg仓库的路径
    """
    out_dicts = list()
    module = get_meta_from_file_name(stt_flt_path_relative_to_cfg.strip())

    for do_dict in in_dicts:
        meta = deepcopy(TMP_META)

        meta["module"] = module

        project, top = get_meta_from_upstream_call(args)
        meta["project"] = project
        meta["top"] = top

        hdl_revision, run_date = get_meta_from_cfi()
        meta["hdl_revision"] = hdl_revision
        meta["run_date"] = run_date

        cfg_revision, update_date, owner = get_meta_from_cfg_repo(args, stt_flt_path_relative_to_cfg)
        meta["cfg_revision"] = cfg_revision
        meta["update_date"] = update_date

        message, owner, reviewers, scheme = get_meta_from_entry(args, do_dict, stt_flt_path_relative_to_cfg)
        meta["message"] = message
        meta["owner"] = owner
        meta["reviewers"] = reviewers
        meta["scheme"] = scheme

        out_dicts.append({"PATTERN": do_dict, "TAG": meta})

    return out_dicts
