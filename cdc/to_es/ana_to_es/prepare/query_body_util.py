# -*- coding:UTF-8 -*-
import pdb
import os, sys
import re
import json
from pprint import pprint
THIS_DIR = os.path.dirname(os.path.realpath(__file__))

sys.path.append(os.path.join(THIS_DIR, "../"))
from ana_file_util import ana_file_to_es_qb


do_status = [
        "bug",
        "verified",
        "pending",
        "uninspected",
        "waived",
]


def a_entry_to_query_body(do_fi_dict):
    """
    当前只接受exclude类型的status/filter
    返回的结果，只考虑路径的统配，这一层不考虑统配结果是要还是不要
    """
    def ignore_entry(key):
        if "status" in do_fi_dict and do_fi_dict["status"] in ["bug", "pending", "uninspected"]:
            return True

    if ignore_entry(do_fi_dict):
        return

    qb = {"query": {"bool": {"should": [], "minimum_should_match": 0}}}
    minimum_should_match = 0
    for key, value in do_fi_dict.items():
        if value != '{}':
            value = re.sub('[\{|\}]', '', value)
            minimum_should_match += 1
            q_key = key
            q_value = value if not (value.startswith('{') and key == 'TAG.schema_name.keyword') else value[1:-1]
            kv_dict = {"wildcard": {q_key: {"value": q_value}}}
            qb["query"]["bool"]["should"].append(kv_dict)
    qb["query"]["bool"]["minimum_should_match"] = minimum_should_match

    return qb


def a_file_to_query_body(file_path):
    """
    一个模块对应一个文件，一般会有多个filter entry
    """
    list_in_query = []
    do_fi_list = ana_file_to_es_qb(file_path)
    for do_fi_dict in do_fi_list:
        qb = a_entry_to_query_body(do_fi_dict)
        if qb:
            # 这种写法的原因：使用该query后留下还没有被分析掉的违例
            list_in_query.append(qb["query"])

    return list_in_query


def multi_module2query_dsl(filter_list, query_container):
    """
    zzzTODO 不要在这个文件里处理这个事情了
    Append filters of modules into one query.
    You must prepare query_container contianer before calling this func.
    """
    for filter_file in filter_list:
        pattern_list = one_file2query_dsl(filter_file)
        q_container["query"]["bool"]["must_not"].extend(pattern_list)    # zzzTODO 这里还没仔细考虑要怎么批处理多模块的，先手动一次次处理

    return True


if __name__ == "__main__":

    q_container = {"query": {"bool": {"must_not": []}}}
    q = multi_module2query_dsl(["tests_orig/input_filter_one_line.tcl", "tests_vio/input_filter_bool.tcl"], q_container)
    pprint(q_container)


    with open("tests_vio/xxx.json", 'w') as fout:
        json.dump(q, fout)

