# -*- coding:UTF-8 -*-
"""
把一个cfg仓库里的一个ana（status/filter）的文件转成 es 的 update_by_query body
"""

from pprint import pprint

import os
import sys
import json
from copy import deepcopy

import logging
logging.basicConfig(level=logging.INFO,
                    format='%(filename)s:%(lineno)d - %(message)s')
logger = logging.getLogger(__name__)


THIS_DIR = os.path.dirname(os.path.realpath(__file__))

sys.path.append(os.path.join(THIS_DIR, "../"))
from query_body_util import a_file_to_query_body
from ana_to_es_doc import get_meta_from_file_name
from ana_to_es_doc import get_meta_from_cfg_repo

sys.path.append(os.path.join(THIS_DIR, "../../../"))
from es_utils.es_connect import get_es


def ana_to_updatebyquery_body(args, ana_file_path, use_mode="keep_excluded"):
    # NOTE 如果是开发环境，这个文件受生成测试用例的影响，根据最新生成的测试用例来修改内部内容
    """
    ana_file_path: 一个cfg仓库里的一个ana的文件（status/filter），但是要用每个entry来标记。因为如果有一个enry导致查询失败，整个文件的所有entry都不生效。
    """
    module = get_meta_from_file_name(ana_file_path)
    _, exc_date, _ = get_meta_from_cfg_repo(args, ana_file_path)

    pattern_list = a_file_to_query_body(ana_file_path)

    # zzzTODO 提前检查一下pattern_list能不能命中vio

    # if use_mode == "keep_excluded":    意思是，status filter 中要exclude的item，在query后被保留下来。使用场景：要看哪个违例被exclude
    # if use_mode == "keep_not_excluded":    意思是，status filter 中要exclude的item，在query后被剔除。使用场景：要看经过分析以后还有多少违例
    to_ret = []
    # 如果不用这个for 弊端：有些filter文件很长，如果某些entry导致文件不能被加载，定位时看到一个超级大的query body。建议改一种写法。
    for pat_one_ana_entry in pattern_list:
        q = {"query": {"bool": {"should" if use_mode == "keep_excluded" else "must_not": [pat_one_ana_entry]}}}
        q.update({
            "script": {
                "source": f"""
if (ctx._source.TAG.containsKey('exc_by_module') && ctx._source.TAG.exc_by_module instanceof List) {{
    ctx._source.TAG.exc_by_module.add('{module}');
}} else {{
    ctx._source.TAG.exc_by_module = ['{module}'];
}}
if (ctx._source.TAG.containsKey('exc_date') && ctx._source.TAG.exc_date instanceof List) {{
    ctx._source.TAG.exc_date.add('{exc_date}');
}} else {{
    ctx._source.TAG.exc_date= ['{exc_date}'];
}}
                """
            }
        })
        logger.debug(json.dumps(q, indent=2))
        to_ret.append(deepcopy(q))

    return to_ret
