
PUT idx_cdc_vio_after_analysis

DELETE idx_cdc_vio_after_analysis

# TX RX 字段按照directive的命名习惯来
# messages owners reviewers cfg_revisions ~的长度保持一致,记录好每次是谁提供了message~ 这样要提供按行级别的unique机制,有点复杂
# messages owners reviewers cfg_revisions 的长度

# 主要是更新这两个字段
#   "exc_by_do": ["filter_id_1", "filter_id_2"],
#   "exc_by_module": ["filter_id_1", "filter_id_2"]

POST idx_cdc_vio_after_analysis/_doc
{
  "TAG": {
    "project": "V_PJT",
    "top": "V_TOP",
    "hdl_revision": "1ab2c3d-changeid-commit-id",
    "rpt_datetime": "2024-04-29T01:02:03+08:00",
    "run_date": "2024-04-29",
    "module": "A->A",
    "cdc_id": "no_sync_38",
    "status": "waived/verified",
    "owner": "a00123456",
    "reviewers": ["a00123456", "c00345678"],
    "exc_by_do": ["filter_id_1", "filter_id_2"],
    "exc_by_module": ["filter_id_1", "filter_id_2"]
  },
  "TX": {
    "soe": "start",
    "clock": "TOP.U_A.U_B.clk_c",
    "signal": "TOP.U_A.U_B.U_C.port_d",
    "file": "/tmpdata/path/to/hdl/rtl.v 12",
    "module": "GIC"
  },
  "RX": {
    "soe": "end",
    "clock": "TOP.U_A.U_B.clk_c",
    "signal": "TOP.U_A.U_B.U_C.port_d",
    "file": "/tmpdata/path/to/hdl/rtl.v 1245",
    "module": "GIC"
  }
}
