# -*- coding:UTF-8 -*-
import os
import sys
THIS_DIR = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(THIS_DIR, "../../../es_utils/"))
from es_connect import get_es
from json2es import serial_index


def get_last_cdc_run_date():
    return "2024-05-01"


def get_cfg_last_cmt_date():
    return "2024-05-10"


q = {
    "query": {
        "bool": {
            "must_not": {
                "exists": {
                    "field": "TAG.exc_by_module"
                }
            }
        }
    }
}


if __name__ == "__main__":
    import argparse
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--env', type=str)
    args = arg_parser.parse_args()

    es = get_es(args.env)
    res = es.count(index="idx_cdc_vio_dtl", body=q)
    res["count"]
    print(res)

    to_index_list = [{
        "cdc_run_date": get_last_cdc_run_date(),
        "update_date": get_cfg_last_cmt_date(),
        "not_exc_num": res["count"],
    }]
    serial_index(es, to_index_list, "idx_cdc_todo_vio_trend")
