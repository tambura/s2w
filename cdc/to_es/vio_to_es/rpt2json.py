# -*- coding:UTF-8 -*-
import pdb

import sys
import json
from dateutil import parser as tp, tz
from datetime import datetime, date

from pydantic import BaseModel

from tqdm import tqdm
import re
from copy import deepcopy
 
from pprint import pprint

import logging

logging.basicConfig(level=logging.ERROR,
                    format='%(filename)s:%(lineno)d - %(message)s')
logger = logging.getLogger(__name__)


file2module = {
    "zen/l3t": "L3",
    "zen/gic": "GIC",
}


# TODO 读一下0in db来获取模块信息
id2module = {}

def which_module_0in(cdc_id):
    if cdc_id is None:
        return None, None
    return id2module[cdc_id]["tx_module"], id2module[cdc_id]["rx_module"]


# RED ONLY from path2module.prj_bin_get_rtl_group import rtl_grp
# def which_module_cfi_r(file_path):
#     tmp_f = re.sub(r'.*?digital_data/rtl/hdl[\w]*', '', file_path, 1)
#     file_grp = "other"
#     for g, p_list in rtl_grp.items():
#         if any(tmp_f.startswith(p) for p in p_list):
#             file_grp = g
#             break
#     return file_grp


def which_module_cfi_y(file_path):
    # TODO 会不会读错rtl路径？
    for part_file_path, module in file2module.items():
        if part_file_path in file_path:
            return module


def get_tag_module(tx_module, rx_module):
    tx_m = tx_module if tx_module else "others"
    rx_m = rx_module if rx_module else "others"
    return tx_m + "->" + rx_m


class TemplateTag(BaseModel):
    # cfi field
    project: str
    top: str
    hdl_revision: str
    rpt_datetime: str
    run_date: str
    module: str    # TX and RX module same, then here
    # 0in fields
    cdc_id: str
    status: str
    # message: str
    # reviewers: str
    # owner: str
    scheme: str
    severity: str

    def set_module(self, module):
        self.module = module


class TemplateTx(BaseModel):
    clock: str
    signal: str
    module: str

    def set_module(self, module):
        self.module = module


class TemplateRx(BaseModel):
    clock: str
    signal: str
    module: str
    # base_type: none,    # optional
    # synchronizer_length: None    # Optional

    def set_module(self, module):
        self.module = module


def ignore_line(line):
    # * 记录一下遇到的各种解析场景

    if re.match(r'^\s*$', line):    # 全是空格的行也跳过
        return True
    if re.match(r'Violations\s+\(\d+\)', line):
        return True
    if re.match(r'Violations', line):
        return True
    if re.match(r'=+$', line):
        return True
    if re.match(r'^.*\(\d+\)\s+\(\w+\)$', line):
        return True
    if re.match(r'-+$', line):
        return True
    if "Base Type" in line:
        return True
    if "Synchronizer length" in line:
        return True
    if re.match(r'Questa CDC.*', line):
        return True
    if re.match(r'Questa Report.*', line):
        return True
    if re.match(r'CDC Report.*', line):
        return True
    if re.match(r'CDC Summary*', line):
        return True
    if re.match(r'^Created .* \d+', line):
        return True
    if re.match(r'^<None>$', line):
        return True
    if re.match(r'^\w+.*\(\d+\)$', line):
        return True
    if re.match(r'.*: diverge :.*', line):
        return True
    if re.match(r'.*Synchronizer ID.*', line):
        return True

    # It is a beginning of a type of violations.
    # Skip these lines:
    #   Single-bit data latch signal across clock domain boundary. (data_latch)
    if re.match(r'.*\(((no_sync)|(combo_logic)|(async_reset)|(async_reset_no_sync)|(bus_two_dff)|(multi_bits)|(fanin_different_clks)|(single_source_reconvergence)|(fifo_memory_ptr_mismatch)|(data_latch))\)$', line):
        return True

    # Unspecified : write pointer : U_A.U_B.... 没有文件路径  暂时跳过
    if re.match(r'.*write pointer.*', line):
        return True


def break_line(line):
    if re.match(r'^Cautions$', line):
        return True
    # if re.match(r'.*(\((no_sync)|(combo_logic)|(async_reset)|(async_reset_no_sync)|(bus_two_dff)|(multi_bits)|(fanin_different_clks)|(single_source_reconvergence))\)$', line):
    #     return True


def is_tx_line(line):
    return re.match(r'^\w+.*', line) and ("start" in line or "end" in line)


def is_rx_line(line):
    return re.match(r'^\s+[\d|\w]+', line) and ("start" in line or "end" in line)


def split_common(line):
    line = line.strip()

    try:
        clock_path = re.findall(r'^.*(?=: start :|: end :)', line)[0].strip()
    except Exception as e:
        logging.error(f"Find clock_path with exception in \"{line}\": {e}")
        exit(1)
        # logging.warning(f"Find clock_path with exception: {e}")
        # clock_path = "null_not_acceptable"

    try:
        port_inst_path = re.findall(r'(?<=art :|end :).*(?= \(/)', line)[0].strip()
        if port_inst_path.startswith("art : ") or port_inst_path.startswith("end: "):
            port_inst_path = port_inst_path.split(':')[1].strip()
    except Exception as e:
        logging.error(f"Find port_inst_path with exception in \"{line}\": {e}")
        exit(1)
        # logging.warning(f"Find port_inst_path with exception: {e}")
        # port_inst_path = "null_not_acceptable"

    try:
        port_file_path = re.findall(r'(/.*: \d+)', line)[0].strip()
    except Exception as e:
        logging.error(f"Find port file path with exception in \"{line}\": {e}")
        exit(1)
        # logging.warning(f"Find port file path with exception: {e}")
        # port_file_path = "null_not_acceptable"

    return clock_path, port_inst_path, port_file_path

 
def split_tx(line):
    vio_id = None
    try:
        vio_id = re.findall(r'\(ID:\s*(.+?)\)', line)[0].strip()
    except Exception as e:
        logging.warning(f"Violation_id not found in TX \"{line}\": {e}")

        # if "Reconvergence" not in line:
        #     logging.warning(f"Find vid with acceptable exception in \"{line}\": {e}")
        #     vio_id = "null_acceptable"
        # else:
        #     logging.error(f"Find vid with NOT acceptable exception in \"{line}\": {e}")
        #     exit(1)

    clock_path, port_inst_path, port_file_path = split_common(line)
    return vio_id, clock_path, port_inst_path, port_file_path


def split_rx(line, vio_id=None):
    """
    vio_id: If got vio_id in tx line, do NOT parse vio_id anymore.
    """
    line = line.strip()
    clock_path, port_inst_path, port_file_path = split_common(line)

    if vio_id is None:
        try:
            # (Synchroizer ID: xxx) should not matched.
            vio_id = re.findall(r'\(ID\s*:\s*(.+?)\)', line)[0].strip()
        except Exception as e:
            logging.error(f"Find vid with NOT acceptable exception in \"{line}\": {e}")
            exit(1)

    try:
        status = re.findall(r'\(Status\s*:\s*(.+)\)', line)[0].strip()
    except Exception as e:
        logging.warning(f"Find status with acceptable exception in \"{line}\": {e}")
        status = "null_acceptable"

    return clock_path, port_inst_path, port_file_path, vio_id, status


def parse_tx(l):
    cur_tx = {}

    if "start" in l:
        cur_tx["soe"] = "start"
    elif "end" in l:
        cur_tx["soe"] = "end"

    vio_id, clock_path, port_inst_path, port_file_path = split_tx(l)
    cur_tx["clock"] = clock_path
    cur_tx["signal"] = port_inst_path
    cur_tx["file"] = port_file_path
    tx_module = "TODO"
    cur_tx["module"] = tx_module if tx_module else "others"
    
    return vio_id, TemplateTx(**cur_tx)


def parse_rx(l, cur_cdc_id):
    soe = None
    cur_rx = {}
    if "start" in l:
        soe = "start"
        RIGHT_BEGIN = True
        clock_path, port_inst_path, port_file_path, vio_id, status = split_rx(l, cur_cdc_id)
        if not all((clock_path, port_inst_path, port_file_path)):
            logger.warning(f"Field missing in {l}")
    elif "end" in l:
        RIGHT_BEGIN = True
        soe = "end"
        clock_path, port_inst_path, port_file_path, vio_id, status = split_rx(l, cur_cdc_id)

    cur_rx["soe"] = soe
    cur_rx["clock"] = clock_path
    cur_rx["signal"] = port_inst_path
    cur_rx["file"] = port_file_path
    rx_module = "TODO"
    cur_rx["module"] = rx_module if rx_module else "others"

    return vio_id, TemplateRx(**cur_rx)


def is_rpt_datetime_line(line):
    find_datetime = re.findall(r'^Created (.*\d{2}:\d{2}:\d{2} \d{4})$', line)
    if len(find_datetime) > 0:
        return find_datetime[0]
    else:
        return None


def load_rpt(rpt_file_name, tag_info, start=0, end=-1):
    with open(rpt_file_name) as fin:
        lines = fin.readlines()

    lines = lines[start: end]
    len_lines = len(lines)

    cur_tag = deepcopy(tag_info)
    cur_tx = None
    cur_rx = None
    cur_cdc_id = None

    RPT_DATETIME = None
    RUN_DATE = None
    TX_KEEP = False

    RX_BEGIN = None
    RX_END = None

    out_json = []

    for lidx, l in tqdm(enumerate(lines)):
        if break_line(l):
            print(l)
            break
        if RPT_DATETIME is None:
            datetime_str = is_rpt_datetime_line(l)
            if datetime_str:
                # rpt_datetime = datetime.strptime(datetime_str, "%a %b %d %H:%M:%S %Y")
                rpt_datetime = tp.parse(datetime_str).astimezone(tz.tzlocal())
                RPT_DATETIME = rpt_datetime.isoformat()
                RUN_DATE = datetime.strftime(rpt_datetime, "%Y-%m-%d")
                cur_tag["rpt_datetime"] = RPT_DATETIME
                cur_tag["run_date"] = RUN_DATE
        if ignore_line(l):
            continue

        if is_tx_line(l):
            if RX_BEGIN is None and RX_END is None:
                # 第一个TX
                TX_KEEP = True
                cur_cdc_id, cur_tx = parse_tx(l)
            elif TX_KEEP == True and RX_BEGIN == True and RX_END == False:
                # 当前TX line表示上一个TX结束，新TX开始
                RX_BEGIN, RX_END = False, True
                TX_KEEP = False
                cur_cdc_id = None
                del cur_tx
                # clean end

                cur_cdc_id, cur_tx = parse_tx(l)
                TX_KEEP = True
        elif is_rx_line(l):
            if RX_BEGIN == False and RX_END == False:
                # RX第一行
                RX_BEGIN, RX_END = True, False
            if RX_BEGIN == True and RX_END == False:
                pass
            cur_cdc_id, cur_rx = parse_rx(l, cur_cdc_id)

            # Got a TX-RX pair/es_doc.
            cur_tag_ = deepcopy(cur_tag)
            cur_tag_["cdc_id"] = deepcopy(cur_cdc_id)

            tx_mod, rx_mod = which_module_0in(cur_cdc_id)
            cur_tx.set_module(tx_mod)
            cur_rx.set_module(rx_mod)
            cur_tag_["module"] = get_tag_module(tx_mod, rx_mod)

            out_json.append({
                "TAG": TemplateTag(**cur_tag_).dict(),
                "TX": cur_tx.dict(),
                "RX": cur_rx.dict(),
            })

        else:
            logging.error(f"Line {lidx+1} is not in all parseing cases: {l}")
            exit(1)

    return out_json


def batch_load_rpt(rpt_file_name, batch_size, start=0, end=-1):
    with open(rpt_file_name) as fin:
        lines = fin.readlines()
    len_lines = len(lines)
    if end == -1:
        end = len_lines

    batch_start_ends = {}
    for s, e in zip(range(start, end, batch_size), range(start+batch_size, end, batch_size)):
        print(s, e)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Convert rpt to json')
    parser.add_argument('--project', type=str)
    parser.add_argument('--top', type=str)
    parser.add_argument('--hdl_rev', type=str)
    parser.add_argument('--rpt_path', type=str)
    parser.add_argument('--json_path', type=str)
    args = parser.parse_args()


    VID_TX = ["single_source_recovergence"]
    VID_RX = ["zzzTODO"]

    tag_info={"project": args.project, "top": args.top, "hdl_revision": args.hdl_rev}
    out_json = load_rpt(args.rpt_path, tag_info, start=0, end=-1)
    with open(args.json_path, 'w') as fout:
        json.dump(out_json, fout, indent=2)

