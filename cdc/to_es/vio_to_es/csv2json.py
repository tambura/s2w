import json
import subprocess
from pprint import pprint
from tqdm import tqdm
from datetime import datetime
from dateutil import parser as tp
from dateutil import tz
from copy import deepcopy

from rpt2json import is_rpt_datetime_line
from rpt2json import TemplateRx, TemplateTag, TemplateTx


def get_date(rpt_path):

    lines = subprocess.check_output(f"head -n100 {rpt_path}", shell=True)
    lines = lines.decode()
    lines = lines.split('\n')
    for l in lines:
        datetime_str = is_rpt_datetime_line(l)
        if datetime_str:
            # rpt_datetime = datetime.strptime(datetime_str, "%a %b %d %H:%M:%S %Y")
            rpt_datetime = tp.parse(datetime_str).astimezone(tz.tzlocal())
            rpt_time = rpt_datetime.isoformat()
            run_date = datetime.strftime(rpt_datetime, "%Y-%m-%d")

            return run_date, rpt_time
 
 
def load_csv(csv_path, tag_info):
    with open(csv_path) as fin:
        lines = fin.readlines()[:-2]    # 首行是表头，末行是数量
 
    out_json = []

    for line in tqdm(lines, ncols=0):
        cdc_id, \
        status, \
        severity, \
        scheme, \
        tx_clock, \
        tx_signal, \
        tx_module, \
        rx_clock, \
        rx_signal, \
        rx_module = line.split('\t')

        tag = deepcopy(tag_info)
        tag.update({"cdc_id": cdc_id.strip(),
                    "status": status.strip(),
                    "severity": severity.strip(),
                    "scheme": scheme.strip(),
                    "module": f"{tx_module.strip()}->{rx_module.strip()}"})
        tx = {"clock": tx_clock.strip(), "signal": tx_signal.strip(), "module": tx_module.strip()}
        rx = {"clock": rx_clock.strip(), "signal": rx_signal.strip(), "module": rx_module.strip()}

    try:
        out_json.append({
            "TAG": TemplateTag(**tag).dict(),
            "TX":  TemplateTx(**tx).dict(),
            "RX":  TemplateRx(**rx).dict(),
        })
    except Exception as e:
        pprint(tag)
        pprint(tx)
        pprint(rx)
        print(e)
        exit(1)

    return out_json


def dump_json(to_dump):
    with open(args.json_path, 'w') as fout:
        json.dump(to_dump, fout, indent=2)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Convert rpt to json')
    parser.add_argument('--project', type=str)
    parser.add_argument('--top', type=str)
    parser.add_argument('--hdl_rev', type=str)
    parser.add_argument('--rpt_path', type=str)
    parser.add_argument('--csv_path', type=str)
    parser.add_argument('--json_path', type=str)
    args = parser.parse_args()


    VID_TX = ["single_source_recovergence"]
    VID_RX = ["zzzTODO"]

    run_date, rpt_time = get_date(args.rpt_path)
    tag_info={"project": args.project,
              "top": args.top,
              "hdl_revision": args.hdl_rev,
              "rpt_datetime": rpt_time,
              "run_date": run_date}
    vio_list = load_csv(args.csv_path, tag_info)
    dump_json(vio_list)
