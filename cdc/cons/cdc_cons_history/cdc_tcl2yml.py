# -*- coding:UTF-8 -*-
import pdb

import os
import sys
import subprocess
import re
import yaml
from copy import deepcopy

import argparse

parser = argparse.ArgumentParser(description='Put old cdc cons to es')
parser.add_argument('--env', type=str)
parser.add_argument('--project', type=str)
parser.add_argument('--top', type=str)
parser.add_argument('--git_commit', type=str)
parser.add_argument('--git_path', type=str)
args = parser.parse_args()


TEMPLATE_TAG_INFO = {
    "TAG_INFO": {
        "PROJECT": args.project,
        "CDC_TOP": args.top,
        "GIT_COMMIT": args.git_commit,
        "GIT_PATH": args.git_path
}

TEMPLATE_ONE_MODULE = {
      "MODULE_NAME": None,
      "CDC_META": None,
      "MODULE_PORTS": []
}

TEMPLATE_ONE_PORT = {
    "PORT_NAME": None,
    "CDC_META": None,
}


def is_skip_line(line):
    if re.match(r'^\s*$', line):
        return True
    if re.match(r'^\s*#.*$', line):
        return True
    if re.search(r'.*synchronizer\s+dff.*', line):
        return True
    if re.search(r'.*cdc\s+report\sscheme.*', line):
        return True


# Module line {


def is_module_line(line):
    find_bbox = re.findall(r'^\s*netlist\s+blackbox\s+(.+?)', line)
    find_synr = re.findall(r'^\s*cdc\s+custom\s+sync\s+(.+?)\s+-type', line)
    find_synr_1 = re.findall(r'^\s*cdc\s+synchronizer\s+custom\s+(.+?)\s+', line)
    return len(find_bbox) > 0 or len(find_synr) > 0


def get_cdc_meta_module(line):
    find_bbox = re.findall(r'^\s*netlist\s+blackbox\s+(.+?)\s*#*$', line)
    find_synr = re.findall(r'^\s*cdc\s+custom\s+sync\s+(.+?)\s+-type', line)
    find_synr_1 = re.findall(r'^\s*cdc\s+synchronizer\s+custom\s+(.+?)\s+', line)
    if len(find_bbox) > 0:
        return "blackbox", find_bbox[0].strip()
    elif len(find_synr) > 0:
        return "synchronizer", find_synr[0].strip()
    elif len(find_synr_1) > 0:
        return "synchronizer", find_synr_1[0].strip()
    else:
        print(f"Should not get cdc meta from line:\n\t{line}")
        exit(1)


# Module line }


# Port line {


def get_module_name(line):
    try:
        module_name = re.findall(r'-module\s+(.*)', line)[0].strip()
    except Exception as e:
        print(f"get_module_name with exception: {e}")
        return None
    return module_name


def get_port_name(line):
    try:
        # (.*) 可能匹配过多
        port_name = re.findall(r'^\s*netlist\s+\w+\s(.+?)\s+-', line)[0].strip()
    except Exception as e:
        print(f"get_port_name with exception: {e}")
        return None
    return port_name


def get_cdc_meta_port(line):
    find_clock  = re.findall(r'^\s*netlist\s+clock\s+(.+?)\s+-module', line)
    find_reset  = re.findall(r'^\s*netlist\s+reset\s+(.+?)\s+-module', line)
    find_ignore = re.findall(r'^\s*netlist\s+port\s+domain\s+(.+?)\s+-ignore', line)
    find_stable = re.findall(r'^\s*cdc\s+signal\s+(.+?)\s+-stable', line)

    find_prt_clk = re.findall(r'^\s*netlist\s+port\s+domain\s+(.+?)\s+-clock', line)
    find_group = re.findall(r'^\s*netlist\s+port\s+domain\s+(.+?)\s+-group', line)
    find_async = re.findall(r'^\s*netlist\s+port\s+domain\s+(.+?)\s+-async', line)

    module_name = get_module_name(line)

    if len(find_clock) > 0:
        return "clock", find_clock[0].strip(), module_name
    elif len(find_reset) > 0:
        return "reset", find_reset[0].strip(), module_name
    elif len(find_ignore) > 0:
        return "ignore", find_ignore[0].strip(), module_name
    elif len(find_stable) > 0:
        return "stable", find_stable[0].strip(), module_name
    elif len(find_prt_clk) > 0:
        return "prt_clk", find_prt_clk[0].strip(), module_name
    elif len(find_group) > 0:
        return "group", find_group[0].strip(), module_name
    elif len(find_async) > 0:
        return "async", find_async[0].strip(), module_name
    else:
        print(f"Line {line} is not parsed!")
        exit(1)


# Port line }


to_yml = {}
to_yml.update(TEMPLATE_TAG_INFO)
to_yml.update({"CDC_METAS": []})
modules_in = []

def add_to_file_yaml(lines):

    def module_only_in_port_line(module_name):
        if module_name not in modules_in:
            new_module = deepcopy(TEMPLATE_ONE_MODULE)
            new_module["MODULE_NAME"] = module_name
            to_yml["CDC_METAS"].append(new_module)

    for lidx, line in enumerate(lines):
        print(f"Parsing line {lidx}")
        if is_skip_line(line):
            continue

        if is_module_line(line):
            bbox_synr, module_name = get_cdc_meta_module(line)

            if module_name not in modules_in:
                modules_in.append(module_name)

                new_module = deepcopy(TEMPLATE_ONE_MODULE)
                new_module["MODULE_NAME"] = module_name
                new_module["CDC_META"] = bbox_synr
                to_yml["CDC_METAS"].append(new_module)
        else:
            # pdb.set_trace()
            cris, port_name, module_name = get_cdc_meta_port(line)

            if cris and port_name and module_name:
                new_port = deepcopy(TEMPLATE_ONE_PORT)
                new_port["PORT_NAME"] = port_name
                new_port["CDC_META"] = cris

                module_only_in_port_line(module_name)

                CONS_TEMPLATE["cdc_metas"][module_name].update({
                    port_name: {"cdc_meta": cris}
                })

    return CONS_TEMPLATE


if __name__ == "__main__":
    with open(sys.argv[1]) as fin:
        lines = fin.readlines()

    cons = add_to_file_yaml(lines)

    with open(sys.argv[2], 'w') as fout:
        yaml.dump(cons, fout)

