# -*- coding:UTF-8 -*-
1# -*- coding:UTF-8 -*-
import pdb
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
import os
1# -*- coding:UTF-8 -*-
import sys
1# -*- coding:UTF-8 -*-
import subprocess
1# -*- coding:UTF-8 -*-
import re
1# -*- coding:UTF-8 -*-
import yaml
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
CONS_TEMPLATE = {
1# -*- coding:UTF-8 -*-
    "project": "",
1# -*- coding:UTF-8 -*-
    "file_path": "",
1# -*- coding:UTF-8 -*-
    "cdc_metas": {
1# -*- coding:UTF-8 -*-
        "MODULE_NAME": {
1# -*- coding:UTF-8 -*-
            "cdc_meta": "BBOX/SYNR?",
1# -*- coding:UTF-8 -*-
            "PORT_NAME": {
1# -*- coding:UTF-8 -*-
                "KEY": "VALUE"
1# -*- coding:UTF-8 -*-
            },
1# -*- coding:UTF-8 -*-
        }
1# -*- coding:UTF-8 -*-
    }
1# -*- coding:UTF-8 -*-
}
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
def is_skip_line(line):
1# -*- coding:UTF-8 -*-
    if re.match(r'^$', line):
1# -*- coding:UTF-8 -*-
        return True
1# -*- coding:UTF-8 -*-
    if re.match(r'^\s*#.*$', line):
1# -*- coding:UTF-8 -*-
        return True
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
# Module line {
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
def is_module_line(line):
1# -*- coding:UTF-8 -*-
    find_bbox = re.findall(r'^\s*netlist\s+blackbox\s+(.+?)', line)
1# -*- coding:UTF-8 -*-
    find_synr = re.findall(r'^\s*cdc\s+custom\s+sync\s+(.+?)\s+-type', line)
1# -*- coding:UTF-8 -*-
    return len(find_bbox) > 0 or len(find_synr) > 0
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
def get_cdc_meta_module(line):
1# -*- coding:UTF-8 -*-
    find_bbox = re.findall(r'^\s*netlist\s+blackbox\s+(.+?)\s*#*$', line)
1# -*- coding:UTF-8 -*-
    find_synr = re.findall(r'^\s*cdc\s+custom\s+sync\s+(.+?)\s+-type', line)
1# -*- coding:UTF-8 -*-
    if len(find_bbox) > 0:
1# -*- coding:UTF-8 -*-
        return "blackbox", find_bbox[0].strip()
1# -*- coding:UTF-8 -*-
    elif len(find_synr) > 0:
1# -*- coding:UTF-8 -*-
        return "synchronizer", find_synr[0].strip()
1# -*- coding:UTF-8 -*-
    else:
1# -*- coding:UTF-8 -*-
        print(f"Should not get cdc meta from line:\n\t{line}")
1# -*- coding:UTF-8 -*-
        exit(1)
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
# Module line }
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
# Port line {
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
def get_module_name(line):
1# -*- coding:UTF-8 -*-
    try:
1# -*- coding:UTF-8 -*-
        module_name = re.findall(r'-module\s+(.*)', line)[0].strip()
1# -*- coding:UTF-8 -*-
    except Exception as e:
1# -*- coding:UTF-8 -*-
        print(f"get_module_name with exception: {e}")
1# -*- coding:UTF-8 -*-
        return None
1# -*- coding:UTF-8 -*-
    return module_name
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
def get_port_name(line):
1# -*- coding:UTF-8 -*-
    try:
1# -*- coding:UTF-8 -*-
        # (.*) 可能匹配过多
1# -*- coding:UTF-8 -*-
        port_name = re.findall(r'^\s*netlist\s+\w+\s(.+?)\s+-', line)[0].strip()
1# -*- coding:UTF-8 -*-
    except Exception as e:
1# -*- coding:UTF-8 -*-
        print(f"get_port_name with exception: {e}")
1# -*- coding:UTF-8 -*-
        return None
1# -*- coding:UTF-8 -*-
    return port_name
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
def get_cdc_meta_port(line):
1# -*- coding:UTF-8 -*-
    find_clock  = re.findall(r'^\s*netlist\s+clock\s+(.+?)\s+-module', line)
1# -*- coding:UTF-8 -*-
    find_reset  = re.findall(r'^\s*netlist\s+reset\s+(.+?)\s+-module', line)
1# -*- coding:UTF-8 -*-
    find_ignore = re.findall(r'^\s*netlist\s+port\s+domain\s+(.+?)\s+-ignore', line)
1# -*- coding:UTF-8 -*-
    find_stable = re.findall(r'^\s*cdc\s+signal\s+(.+?)\s+-stable', line)
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
    find_group = re.findall(r'^\s*netlist\s+port\s+domain\s+(.+?)\s+-group', line)
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
    module_name = get_module_name(line)
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
    if len(find_clock) > 0:
1# -*- coding:UTF-8 -*-
        return "clock", find_clock[0].strip(), module_name
1# -*- coding:UTF-8 -*-
    elif len(find_reset) > 0:
1# -*- coding:UTF-8 -*-
        return "reset", find_reset[0].strip(), module_name
1# -*- coding:UTF-8 -*-
    elif len(find_ignore) > 0:
1# -*- coding:UTF-8 -*-
        return "ignore", find_ignore[0].strip(), module_name
1# -*- coding:UTF-8 -*-
    elif len(find_stable) > 0:
1# -*- coding:UTF-8 -*-
        return "stable", find_stable[0].strip(), module_name
1# -*- coding:UTF-8 -*-
    elif len(find_group) > 0:
1# -*- coding:UTF-8 -*-
        return "group", find_group[0].strip(), module_name
1# -*- coding:UTF-8 -*-
    else:
1# -*- coding:UTF-8 -*-
        print(f"Line {line} is not parsed!")
1# -*- coding:UTF-8 -*-
        exit(1)
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
# Port line }
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
def add_to_file_yaml(lines):
1# -*- coding:UTF-8 -*-
    for line in lines:
1# -*- coding:UTF-8 -*-
        if is_skip_line(line):
1# -*- coding:UTF-8 -*-
            continue
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
        if is_module_line(line):
1# -*- coding:UTF-8 -*-
            bbox_synr, module_name = get_cdc_meta_module(line)
1# -*- coding:UTF-8 -*-
            if module_name not in CONS_TEMPLATE["cdc_metas"]:
1# -*- coding:UTF-8 -*-
                CONS_TEMPLATE["cdc_metas"].update({module_name: {
1# -*- coding:UTF-8 -*-
                    "cdc_meta": bbox_synr,
1# -*- coding:UTF-8 -*-
                }})
1# -*- coding:UTF-8 -*-
        else:
1# -*- coding:UTF-8 -*-
            # pdb.set_trace()
1# -*- coding:UTF-8 -*-
            cris, port_name, module_name = get_cdc_meta_port(line)
1# -*- coding:UTF-8 -*-
            if cris and port_name and module_name:
1# -*- coding:UTF-8 -*-
                CONS_TEMPLATE["cdc_metas"][module_name].update({
1# -*- coding:UTF-8 -*-
                    port_name: {"cdc_meta": cris}
1# -*- coding:UTF-8 -*-
                })
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
    return CONS_TEMPLATE
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
if __name__ == "__main__":
1# -*- coding:UTF-8 -*-
    with open(sys.argv[1]) as fin:
1# -*- coding:UTF-8 -*-
        lines = fin.readlines()
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
    cons = add_to_file_yaml(lines)
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
    with open(sys.argv[2], 'w') as fout:
1# -*- coding:UTF-8 -*-
        yaml.dump(cons, fout)
1# -*- coding:UTF-8 -*-

