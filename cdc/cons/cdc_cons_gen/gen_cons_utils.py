# -*- coding:UTF-8 -*-
import os
import re
from rich.console import Console
from rich.table import Table


"""
目前有9列，为了最后的 -module XXX 对齐，不满9列的请insert empty str
PORT_NAME @col4, MODULE_NAME @col9
"""
grid_map = {
        "clk": [
            "netlist",
            "clock",
            "",
            "CLK_NAME",
            "",
            "-group",
            "CLK_GRP_NAME",
            "-module",
            "MDL_NAME",
         ],
         "rst_clk": [
            "netlist",
            "port",
            "domain",
            "RST_NAME",
            "",
            "-clock",
            "CLK_NAME",
            "-module",
            "MDL_NAME",
        ],
         "rst": [
            "netlist",
            "reset",
            "",    # "-async", 如果有了 -group 就不需要 -async 了，因为 -async 声明的是该rst和其他port都是异步的，这个和crg xls中想表达的含义相反
            "RST_NAME",
            "-active_low",
            "-group",
            "CLK_GRP_NAME",
            "-module",
            "MDL_NAME",
        ],
         "stb": [
            "netlist",
            "port",
            "domain",
            "PRT_NAME",
            "-ignore",
            "",
            "",
            "-module",
            "MDL_NAME",
        ],
         "ign": [
            "cdc",
            "signal",
            "",
            "PRT_NAME",
            "-stable",
            "",
            "",
            "-module",
            "MDL_NAME",
        ],
}


def get_mdl_cons_grid():
    table = Table(show_header=False, box=None)
    table.add_column("C1", justify="left")
    table.add_column("C2", justify="left")
    table.add_column("C3", justify="left")
    table.add_column("C4", justify="left")
    table.add_column("C5", justify="left")
    table.add_column("C6", justify="left")
    table.add_column("C7", justify="left")
    table.add_column("C8", justify="left")
    table.add_column("C9", justify="left")
    return table


def add_table_row(table, port_type, col_template, is_cmt, **kwargs):
    # If match a upper case word, it should take value from the kwargs.
    # *[] add_row() not accept list, convert list.
    cols_one_row = [kwargs[c.lower()] if re.match(r'^[A-Z].*', c) else c for c in col_template]
    if is_cmt:
        cols_one_row.insert(0, "#")
    else:
        cols_one_row.insert(0, " ")
    table.add_row(*cols_one_row)


def table_to_file(table, new_file_flag, file_path):
    """
    table: 
    new_file_flag: If python gen_cons.py, create new file at the first module and then amend to it.

    """
    # NOTE Rule 1: One table, one module.
    # NOTE Rule 2: One table, one write.
    with open(file_path, 'w' if new_file_flag else 'a') as fout:
        console = Console(file=fout)
        console.print(table)

def table_to_stdout(table):
    console = Console()
    console.print(table)


if __name__ == "__main__":
    # add_table_clk_row(table_clk, "CLK_NAME", "CLK_GRP_NAME", "MDL_NAME")
    # add_table_clk_row(table_clk, "CLK_NAMEE", "CLK_GRP_NAME", "MDL_NAME", False)
    # add_table_clk_row(table_clk, "CLK_NAMEEE", "CLK_GRP_NAME", "MDL_NAME")
    # add_table_clk_row(table_clk, "CLK_NAMEEEEEE", "CLK_GRP_NAME", "MDL_NAME", False)
    # add_table_rst_row(table_rst, "RST_NAME", "CLK_GRP_NAME", "MDL_NAME")
    # add_table_stb_row(table_stb, "PRT_NAME", "MDL_NAME")
    # add_table_ign_row(table_ign, "PRT_NAME", "MDL_NAME")

    table = get_mdl_cons_grid()
    # table.add_row(*["c1", "c2"])
    # prt_name = "test_prt"
    # mdl_name = "test_mdl"
    # add_table_row(table, "ign", grid_map["ign"], False, prt_name, mdl_name)

    table_to_file(table, "table.tcl")

