# -*- coding:UTF-8 -*-
import sys
from openpyxl import load_workbook
import yaml
import logging


name2colomn = {
        "crg_name": "A",    # A
        "module_name": "C",    # C
        "clk_src": "D",    # D
        "clk_div": "E",    # E
        "clk_div1": "F",    # F
        "clk_port_name": "G",    # G
        "clk_name_custom": "H",    # H
        "rst_port_name": "Y",    # Y
        "rst_name_custom": "Z",    # Z
        "rst_src": "AA",
        }

"""
clk_port_name, rst_port_name 二者有一合法即可生成一个yml entry，这样可以让yml entry和xls row对应上
"""
TEMPLATE_CRG = {
        "crg_name": "",    # A
        "module_name": "",    # C
        "clk_src": "",    # D
        "clk_div": "",    # E
        "clk_div1": "",    # F
        "clk_port_name": "",    # G
        "clk_name_custom": "",    # H
        "rst_port_name": "",    # Y
        "rst_name_custom": "",    # Z
        "rst_src": "",    # AA
        }


def get_ws_max_row_idx(ws):
    # Lookahead 10 rows.
    cur_row_idx = 1
    MAY_EOR = False
    RLY_EOR = False
    may_eor_idx = -1
    rly_eor_idx = -1
    while (MAY_EOR is not True or RLY_EOR is not True):
        if ws[f"{name2colomn['crg_name']}{cur_row_idx}"].value:
            if MAY_EOR is True and cur_row_idx - may_eor_idx < 10:
                MAY_EOR = False
            cur_row_idx += 1
        elif MAY_EOR is False:
            MAY_EOR = True
            may_eor_idx = cur_row_idx
            cur_row_idx += 1
        elif MAY_EOR is True:
            if ws[f"{name2colomn['crg_name']}{cur_row_idx}"].value:
                MAY_EOR = False
                cur_row_idx += 1
            elif cur_row_idx - may_eor_idx < 10:    # 多看10行，如果10行以内有任意行出现非空的，都是假的End of Rows
                cur_row_idx += 1
            else:
                RLY_EOR = True
                rly_eor_idx = may_eor_idx
                break
    return rly_eor_idx

def is_valid_row(ws, row_idx):
    """
    clk_port_name, rst_port_name 二者有一合法即可生成一个yml entry，这样可以让yml entry和xls row对应上
    """
    return True \
        and ws[f"{name2colomn['crg_name'       ]}{row_idx}"].value is not None \
        and ws[f"{name2colomn['module_name'    ]}{row_idx}"].value is not None \
        and ws[f"{name2colomn['clk_src'        ]}{row_idx}"].value is not None \
        and ws[f"{name2colomn['clk_div'        ]}{row_idx}"].value is not None \
        and ws[f"{name2colomn['clk_port_name'  ]}{row_idx}"].value is not None \
        or  ws[f"{name2colomn['clk_name_custom']}{row_idx}"].value is not None \
        or  ws[f"{name2colomn['rst_port_name'  ]}{row_idx}"].value is not None \
        or  ws[f"{name2colomn['rst_name_custom']}{row_idx}"].value is not None 

ALL_CRG = []
CLK_DOMAINS_IN_USE = []


def get_cell_or_null(cell_value):
    return cell_value.strip() if cell_value else None


wb = load_workbook(sys.argv[1])
ws = wb["crg"]
max_row_idx = get_ws_max_row_idx(ws)
for row_idx in range(4, max_row_idx):    # zzzTODO HARDCODE
    print(f"Info: process xls row: {row_idx} ...")
    if is_valid_row(ws, row_idx): 
        one_crg = {
                "crg_name":                         ws[f"{name2colomn['crg_name'       ]}{row_idx}"].value.strip(),    # A
                "module_name":                      ws[f"{name2colomn['module_name'    ]}{row_idx}"].value.strip(),    # C
                "clk_src":                          ws[f"{name2colomn['clk_src'        ]}{row_idx}"].value.strip(),    # D
                "clk_div":                          ws[f"{name2colomn['clk_div'        ]}{row_idx}"].value.strip(),    # E
                "clk_div1":        get_cell_or_null(ws[f"{name2colomn['clk_div1'       ]}{row_idx}"].value),           # F
                "clk_port_name":   get_cell_or_null(ws[f"{name2colomn['clk_port_name'  ]}{row_idx}"].value),           # G
                "clk_name_custom": get_cell_or_null(ws[f"{name2colomn['clk_name_custom']}{row_idx}"].value),           # H
                "rst_port_name"  : get_cell_or_null(ws[f"{name2colomn['rst_port_name'  ]}{row_idx}"].value),           # Y
                "rst_name_custom": get_cell_or_null(ws[f"{name2colomn['rst_name_custom']}{row_idx}"].value),           # Z
                "rst_src"        : get_cell_or_null(ws[f"{name2colomn['rst_src'        ]}{row_idx}"].value),           # AA
        }

        if one_crg['clk_src'] is None or one_crg['clk_div'] is None:
            logging.error(f"Invalid clk_src or clk_div: {one_crg}")
            exit(1)
        cur_clk_domain = f"{one_crg['clk_src']}_{one_crg['clk_div']}"
        if cur_clk_domain not in CLK_DOMAINS_IN_USE:
            CLK_DOMAINS_IN_USE.append(cur_clk_domain)
        one_crg.update({"clk_group": cur_clk_domain})

        ALL_CRG.append(one_crg)


with open(sys.argv[2], 'w') as fout:
    yaml.dump(ALL_CRG, fout)

