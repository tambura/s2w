# -*- coding:UTF-8 -*-
1# -*- coding:UTF-8 -*-
import pdb
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
import os
1# -*- coding:UTF-8 -*-
import sys
1# -*- coding:UTF-8 -*-
import subprocess
1# -*- coding:UTF-8 -*-
import re
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
def is_clock_line(line):
1# -*- coding:UTF-8 -*-
    # 约束前面可能有若干空格
1# -*- coding:UTF-8 -*-
    return re.match(r'^\s*netlist\s+clock\s+.*', line)
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
def is_reset_line(line):
1# -*- coding:UTF-8 -*-
    return re.match(r'^\s*netlist\s+reset\s+.*', line)
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
def get_module_name(line):
1# -*- coding:UTF-8 -*-
    return re.findall(r'-module\s+(.*)', line)[0].strip()
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
def get_port_name(line):
1# -*- coding:UTF-8 -*-
    # (.*) 可能匹配过多
1# -*- coding:UTF-8 -*-
    return re.findall(r'^\s*netlist\s+\w+\s(.+?)\s+-', line)[0].strip()
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
def find_ports_file(ports_info_dir, module_name):
1# -*- coding:UTF-8 -*-
    cmd = f"grep -rnw {module_name} {ports_info_dir} | awk -F ':' '{{print $1;}}'"
1# -*- coding:UTF-8 -*-
    grep_out = subprocess.check_output(cmd, shell=True)
1# -*- coding:UTF-8 -*-
    grep_out = grep_out.decode().strip()
1# -*- coding:UTF-8 -*-
    if grep_out == '':
1# -*- coding:UTF-8 -*-
        print(f"Ports info not found for module: {module_name}")
1# -*- coding:UTF-8 -*-
        return
1# -*- coding:UTF-8 -*-
    else:
1# -*- coding:UTF-8 -*-
        # 如果找到多个文件，是同一个module例化了多处，取一个即可
1# -*- coding:UTF-8 -*-
        return grep_out.split('\n')[0]
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
def is_real_port(ports_info_file, port_name, module_name):
1# -*- coding:UTF-8 -*-
    # pdb.set_trace()
1# -*- coding:UTF-8 -*-
    grep_out = subprocess.check_output(f"grep -w {port_name} {ports_info_file}", shell=True)
1# -*- coding:UTF-8 -*-
    if grep_out != '':
1# -*- coding:UTF-8 -*-
        print(f"Port '{port_name}' is port of module '{module_name}'")
1# -*- coding:UTF-8 -*-
        return True
1# -*- coding:UTF-8 -*-
    else:
1# -*- coding:UTF-8 -*-
        print(f"Port '{port_name}' is not port of module '{module_name}'")
1# -*- coding:UTF-8 -*-
        return False
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
def verify_port_in_module(lines):
1# -*- coding:UTF-8 -*-
    for line in lines:
1# -*- coding:UTF-8 -*-
        if is_clock_line(line) or is_reset_line(line):
1# -*- coding:UTF-8 -*-
            mdl_name = get_module_name(line)
1# -*- coding:UTF-8 -*-
            prt_name = get_port_name(line)
1# -*- coding:UTF-8 -*-
            ports_info_dir = sys.argv[2]
1# -*- coding:UTF-8 -*-
            ports_info_file = find_ports_file(ports_info_dir, mdl_name)
1# -*- coding:UTF-8 -*-
            if ports_info_file is None:
1# -*- coding:UTF-8 -*-
                exit(1)
1# -*- coding:UTF-8 -*-
            # pdb.set_trace()
1# -*- coding:UTF-8 -*-
            if not is_real_port(ports_info_file, prt_name, mdl_name):
1# -*- coding:UTF-8 -*-
                exit(1)
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
if __name__ == "__main__":
1# -*- coding:UTF-8 -*-
    with open(sys.argv[1]) as fin:
1# -*- coding:UTF-8 -*-
        lines = fin.readlines()
1# -*- coding:UTF-8 -*-

1# -*- coding:UTF-8 -*-
    verify_port_in_module(lines)
1# -*- coding:UTF-8 -*-

