# -*- coding:UTF-8 -*-
import yaml


# 这个只作为参考，里面有大量的未定的端口
def load_get_mod_io_ports(mdl_ports_path):
    def is_valid_line(line):
        if re.match(r'^Module: .*\(.*\)$', line):
            return True
        elif re.match(r'\s+\d+\)\s(Input)|(Output).*', line):
            return True
    
    def get_module_name(line):
        re_s_rtn = re.search(r'Module: .*\((.+?)\)', line)
        if re_s_rtn:
            return re_s_rtn.group[1]
        else:
            logging.error(f"Not found: valid module name in valid line of ports of: {line}")
            exit(1)

    def get_port_name(line):
        re_s_rtn = re.search(r'(?<=:\s).*(?=\s+\(Size:)', line)
        if re_s_rtn:
            return re_s_rtn.group[0]
        else:
            logging.error(f"Not found: valid port name in valid line of ports of: {line}")
            exit(1)

    with open(mdl_ports_path) as fin:
        lines = fin.readlines()

    module_name = None
    # ports = []
    # for line in lines:
    #     if is_valid_line(line):
    #         if module_name is None:
    #             module_name = get_module_name(line)
    #         port_name = get_port_name(line)
    
    return {module_name: ["# " + l for l in lines]}


def collect_mdl2prt_map(ports_path):
    all_mdl2prt = {}
    for mdl_ports_path in os.list_dir(ports_path):
        all_mdl2prt.update(load_get_mod_io_ports(mdl_ports_path))

    return all_mdl2prt


collect_mdl2prt_map("")
