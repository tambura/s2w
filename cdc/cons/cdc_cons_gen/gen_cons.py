# -*- coding:UTF-8 -*-
import sys
import re
from gen_cons_utils import grid_map
from gen_cons_utils import add_table_row
from gen_cons_utils import table_to_file, table_to_stdout
from gen_cons_utils import get_mdl_cons_grid
import yaml
from pprint import pprint
import logging


def load_die_crg_gen_yml(yml_path):
    with open(yml_path) as fin:
        return yaml.load(fin, Loader=yaml.FullLoader)


def get_clk_grp_by(die_crg_mdl_yml, crg_mdl_name, crg_prt_name):
    for crg in die_crg_mdl_yml:
        if crg["crg_name"] == crg_mdl_name and crg["clk_port_name"] == crg_prt_name:
            return crg["clk_group"]


def load_get_mod_io_ports(crg_mdl_yml_path, ports_path):
    def is_valid_line(line):
        if re.match(r'^Module: .*\(.*\)$', line):
            return True
        elif re.match(r'\s+\d+\)\s(Input)|(Output).*', line):
            return True
    
    def get_module_name(line):
        re_s_rtn = re.search(r'Module: .*\((.+?)\)', line)
        if re_s_rtn:
            return re_s_rtn.group[1]
        else:
            logging.error(f"Not found: valid module name in valid line of ports of: {line}")
            exit(1)

    def get_port_name(line):
        re_s_rtn = re.search(r'(?<=:\s).*(?=\s+\(Size:)', line)
        if re_s_rtn:
            return re_s_rtn.group[0]
        else:
            logging.error(f"Not found: valid port name in valid line of ports of: {line}")
            exit(1)

    with open(ports_path) as fin:
        lines = fin.readlines()
    module_name = None
    for line in lines:
        if is_valid_line(line):
            if module_name is None:
                module_name = get_module_name(line)

                return {module_name: ["# "+l for l in lines]}
            # port_name = get_port_name(line)


def split_custom(custom_cell):
    if custom_cell is None:
        return None

    custom_cell = custom_cell.replace(',', ':')
    return custom_cell.split(':')


def find_clk_for_rst(crg, map_crg2pin, mode):
    """
    crg: crg dict in yml, same as a crg row in xls.
    clk_of_grp: 如果表格中的rst所在的行无clk，找到它所在的时钟分组，然后找其中一个clk pin即可
    return: {rst_name: clk_name} if valid. None if invalid.
    """
    def get_map_crg2pin(crg_name, clk_grp_name):
        if crg_name in map_crg2pin and clk_grp_name in map_crg2pin[crg_name]:
            return map_crg2pin[crg_name][clk_grp_name][0][4:]    # 4: 如果是捞回来的，去掉clk_前缀，统一在外面加
        else:
            return

    def build_return(rst_or_rsts, clk_port):
        if clk_port is None:
            return    # 重要！不要返回 {rst_port: None}
        if type(rst_or_rsts) is list:
            return {rst: clk_port for rst in rst_or_rsts}
        elif type(rst_or_rsts) is str:
            return {rst_or_rsts: clk_port}
        else:
            return    # 会在外层被处理成注释掉的行

    if mode == "rst_port_name":
        if crg["clk_port_name"]:
            assert len(crg["clk_port_name"].split(',')) == 1, 'crg["clk_port_name"] must not have ,'
            assert len(crg["rst_port_name"].split(',')) == 1, 'crg["rst_port_name"] must not have ,'

            return build_return(crg["rst_port_name"], crg["clk_port_name"])
        else:
            clk_of_rst = get_map_crg2pin(crg["crg_name"], crg["clk_group"])
            return build_return(crg["rst_port_name"], clk_of_rst)
    elif mode == "rst_name_custom":
        rsts = split_custom(crg["rst_name_custom"])
        if crg["clk_port_name"]:
            return build_return(split_custom(crg["clk_name_custom"]), crg["clk_port_name"])
        elif crg["clk_name_custom"]:
                clks = split_custom(crg["clk_name_custom"])
                # TODO not checking: rst_name is matched with clk_name, fuzzy match may be needed.
                return build_return(rsts, clks[0])
        else:
            # Case 1: len(clks) != len(rsts)
            # Case 2: one of clk/rst != null but another == null
            clk_of_rst = get_map_crg2pin(crg["crg_name"], crg["clk_group"])
            return build_return(rsts, clk_of_rst)
    else:
        return None

FLAG_NEW_CONS_FILE = True

# xls中有些rst port所在行的clk port留空，留一个map来查这种rst port的cls port是什么
# {crg_name: {clk_grp: [clk_pins]}}
MAP_CRG2PIN = {}
def add_to_map_crg2pin(crg_name, clk_grp, clk_port):
    if crg_name not in MAP_CRG2PIN:
        MAP_CRG2PIN.update({crg_name: {clk_grp: [clk_port]}})
    else:
        if clk_grp not in MAP_CRG2PIN[crg_name]:
            MAP_CRG2PIN[crg_name].update({clk_grp: [clk_port]})
        else:
            MAP_CRG2PIN[crg_name][clk_grp].append(clk_port)


for crg in load_die_crg_gen_yml(sys.argv[1]):
    table = get_mdl_cons_grid()

    # * Gen `netlist clock`
    def add_clk_pre(name_in_xls):
        print("clk_" + name_in_xls)
        return "clk_" + name_in_xls

    # ** crg["clk_port_name"]
    if crg["clk_port_name"]:
        clk_prt_name = add_clk_pre(crg["clk_port_name"])
        add_table_row(table, "clk", grid_map["clk"],
                      is_cmt=False,
                      clk_name=clk_prt_name,
                      clk_grp_name=crg["clk_group"],
                      mdl_name=crg["crg_name"])
        add_to_map_crg2pin(crg["crg_name"], crg["clk_group"], clk_prt_name)
    else:
        pass    # yml中允许crg["clk_port_name"]为空，因为crg["clk_name_custom"]可能非空
    # ** crg["clk_name_custom"] may have multi clk ports
    if split_custom(crg["clk_name_custom"]):
        for port in split_custom(crg["clk_name_custom"]):
            if port is not None:
                clk_prt_name = add_clk_pre(port)
                add_table_row(table, "clk", grid_map["clk"], False,
                        clk_name=clk_prt_name,
                        clk_grp_name=crg["clk_group"],
                        mdl_name=crg["crg_name"])
                add_to_map_crg2pin(crg["crg_name"], crg["clk_group"], clk_prt_name)

    # * Gen `netlist reset`
    def add_rst_pre_pst(name_in_xls):
        return 'rst_' + \
                name_in_xls + \
                ('_power_n' if crg['rst_src'] == 'power' else '_n')

    # ** crg["rst_port_name"]
    if crg["rst_port_name"]:
        rst_name_yml = crg["rst_port_name"]
        rst_prt_name = add_rst_pre_pst(rst_name_yml)
        clk_of_rst = find_clk_for_rst(crg, MAP_CRG2PIN, mode="rst_port_name")
        print(f"DEBUG: clk_of_rst must not be xxx: None: {clk_of_rst}")
        if clk_of_rst:
            add_table_row(table, "rst_clk", grid_map["rst_clk"],
                          is_cmt=False,
                          rst_name=rst_prt_name,
                          clk_name=add_clk_pre(clk_of_rst[rst_name_yml]),
                          mdl_name=crg["crg_name"])
        else:    # This is a comment in cons tcl. Because of valid no valid clk.
            add_table_row(table, "rst_clk", grid_map["rst_clk"],
                          is_cmt=True,
                          rst_name=rst_prt_name,
                          clk_name="TODO",
                          mdl_name=crg["crg_name"])
        # This is setting reset group of reset. Not clock group of reset.
        # add_table_row(table, "rst", grid_map["rst"], False,
        #         rst_name=rst_prt_name,
        #         clk_grp_name=crg["clk_group"],
        #         mdl_name=crg["crg_name"])
    else:
        pass    # yml中允许crg["rst_port_name"]为空，因为crg["rst_name_custom"]可能非空
 
    # ** crg["rst_name_custom"] may have multi rst ports
    # TODO hicl的rst_name_custom非空，clk_name_custom为空，clk_port_name非空，生成了注释，待解决
    clks_of_rsts = find_clk_for_rst(crg, MAP_CRG2PIN, mode="rst_name_custom")
    if split_custom(crg["rst_name_custom"]):
        for port in split_custom(crg["rst_name_custom"]):
            if port is not None:
                rst_name_yml = port
                rst_prt_name = add_rst_pre_pst(rst_name_yml)
                if clks_of_rsts and rst_name_yml in clks_of_rsts:
                    add_table_row(table, "rst_clk", grid_map["rst_clk"], False,
                            rst_name=rst_prt_name,
                            clk_name=add_clk_pre(clks_of_rsts[rst_name_yml]),
                            mdl_name=crg["crg_name"])
                else:    # This is a comment in cons tcl. Because of valid no valid clk.
                    add_table_row(table, "rst_clk", grid_map["rst_clk"], True,
                            rst_name=rst_prt_name,
                            clk_name="TODO",
                            mdl_name=crg["crg_name"])

    # * Gen cdc signal stable
    # add_table_row(table, "stb", grid_map["stb"], False, prt_name, mdl_name)

    # * Gen ignore
    # add_table_row(table, "ign", grid_map["ign"], False, prt_name, mdl_name)

    table_to_file(table, FLAG_NEW_CONS_FILE, sys.argv[2])
    FLAG_NEW_CONS_FILE = False
    # table_to_stdout(table)

    with open(sys.argv[2], 'a') as f:
        f.write("\n\n")

