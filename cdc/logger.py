import logging
logging.basicConfig(level=logging.INFO,
                    format='%(filename)s:%(lineno)d - %(message)s')
logger = logging.getLogger(__name__)
