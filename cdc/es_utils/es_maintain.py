# -*- coding:UTF-8 -*-
from es_connect import get_es
import argparse
import json


parser = argparse.ArgumentParser(description='这是一个使用 argparse 的示例程序。')

parser.add_argument('--env', type=str)
parser.add_argument('--delete', action='store_true',
                    help='Used in delete index')
parser.add_argument('--create', action='store_true',
                    help='Used in create index')
parser.add_argument('--index', type=str)

args = parser.parse_args()


if args.index == "idx_cdc_vio":
    with open("idx_cdc_vio.json") as fin:
        index_mappings = json.load(fin)
    with open("idx_cdc_vio_set.json") as fin:
        index_settings = json.load(fin)


if args.index == "idx_cdc_cons_history":
    with open("idx_cdc_cons_history.json") as fin:
        index_mappings = json.load(fin)


ES = get_es(args.env)

if args.create:
    try:
        response = ES.indices.create(index=args.index,
                settings=index_settings, mappings=index_mappings)
                # body={"settings": index_settings, "mappings": index_mappings})
        print(f"The rsp of put index is {response}")
    except Exception as e:
        print(f"Exception of put index {args.index}: {e}")
elif args.delete:
    try:
        response = ES.indices.delete(index=args.index)
        print(f"The rsp of del index is {response}")
    except Exception as e:
        print(f"Exception of put index {args.index}: {e}")

