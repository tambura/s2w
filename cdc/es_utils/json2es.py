# -*- coding:UTF-8 -*-
import sys
import os
from pprint import pprint
from tqdm import tqdm

import json

from elasticsearch import Elasticsearch
from elasticsearch import helpers

THIS_DIR = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(THIS_DIR, "../es_utils/"))
from es_connect import get_es
import logging


logging.basicConfig(level=logging.INFO,
                    format='%(filename)s:%(lineno)d - %(message)s')
logger = logging.getLogger(__name__)


# mapping = ES.indices.get_mapping(index=args.index)
# print(mapping)


# actions = [{
#             "_index": "test_cdc",
#             "_source": {}
# }]
# helpers.bulk(es, actions)


def bulk_index(es, to_index_json: list, bulk_size: int, index: str):
    """
    to_index_json: list of dict
    """
    to_index_len = len(to_index_json)
    assert bulk_size <= to_index_len, "bulk_size < to_index_len"
    actions = None
    logger.debug(f"bulk_size = {bulk_size}")
    for batch_b, batch_e in tqdm(zip(range(0, to_index_len, bulk_size), (bulk_size, to_index_len+1, bulk_size)), ncols=0):
        logger.debug(f"batch_b = {batch_b}, batch_e = {batch_e}")
        del actions
        actions = [{"_index": index, "_source": doc}
                   for doc in to_index_json[batch_b: batch_e]]
        helpers.bulk(es, actions)


def dump_cursor(cursor):
    tgt_f = "cursor_tx_rx.log"
    if os.path.exists(tgt_f):
        with open(tgt_f, 'a') as fout:
            fout.write(str(cursor) + '\n')
    else:
        with open(tgt_f, 'w') as fout:
            fout.write(str(cursor) + '\n')


def load_cursor():
    tgt_f = "cursor_tx_rx.log"
    if os.path.exists(tgt_f):
        with open(tgt_f, 'r') as fin:
            try:
                return int(fin.readlines()[-1].strip())
            except IndexError:
                return 0
    else:
        return 0


def serial_index(es, to_index_list, index):
    max_cursor = len(to_index_list)
    cur_cursor = load_cursor()
    for cursor in range(cur_cursor, max_cursor):
        cur_cursor = cursor
        # print(f"Debug: cur_cursor of list(not rpt) = {cur_cursor}")
        tx_rx = to_index_list[cursor]
        try:
            response = es.index(index=index, body=tx_rx)
            if response["result"] == "created":
                logger.info(f"Index doc pass: {tx_rx}")
            else:
                logger.error(f"Index doc fail: {tx_rx}")
                dump_cursor(cur_cursor)
                exit(1)
        except Exception as e:
            logger.warning(e)
    dump_cursor(cur_cursor)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Put docs to es')
    parser.add_argument('--env', type=str)
    parser.add_argument('--json', type=str)
    parser.add_argument('--index', type=str)
    parser.add_argument('--bulk', action='store_true')
    parser.add_argument('--batch_size', type=int)
    args = parser.parse_args()

    es = get_es(args.env)

    # pprint(to_index_json)
    # serial_index(es, to_index_json, args.index)

    with open(args.json) as fin:
        bulk_index(es, json.load(fin), args.batch_size, args.index)

