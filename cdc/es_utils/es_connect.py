# -*- coding:UTF-8 -*-
from elasticsearch import Elasticsearch
from elasticsearch import helpers


def get_es(env):
    if   env == "lg":
        host = "localhost"
    elif env == "lj":
        host = "192.168.8.100"
    if   env == "tg":
        host = "100.88.26.52"
    elif env == "r":
        host = "szvictexe03-hs"
    else:
        host = "localhost"

    es = Elasticsearch(
            hosts=[
                {
                    "host": host,
                    "port": 9200,
                    "scheme": "http"
                }
            ])
    return es

