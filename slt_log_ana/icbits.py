from copy import deepcopy


class IcBits(object):
    _hex = None
    _dec = None
    _bin = None
    _bit_list = None  # first item is the least significant bit
    is_lit = True  # Little endian.
    length = 0  # length in bit.

    def __init__(self, src):
        if type(src) is str:
            if src[0:2] == "0x":  # NOTE: src must be strip()
                self.length = (len(src) - 2) * 4
                src = int(src, base=16)
            elif src[0:2] == "0b":
                self.length = len(src) - 2
                src = int(src, base=2)
            else:
                self.length = len(bin(int(src))) - 2
                src = int(src)
        elif type(src) == list:
            if type(src[-1]) == str:
                self.length = len(src)
                src = int(list2hex(src), base=16)
            elif type(src[-1]) == IcBits:  # This is concatenate operator.
                # For constraint: tuple items must be IcBits instance.
                for i in src:
                    self.length += i.length
                self.concat(self.length, src)
                src = list2dec(self._bit_list)
        else:  # Need set length
            self.length = len(bin(src)) - 2  # exclude the prefix 0b
        if type(src) is int:
            pass

        self._dec = src
        self._hex = hex(self._dec)

        self._bin = bin(self._dec)
        if len(self._bin) - 2 < self.length:  # in case that ['1', '0'] will get '0b1'
            self._bin = '0b' + (self.length - len(self._bin) + 2) * '0' + self._bin[2:]    # +2 is 0b

        self._bit_list = dec2list(self._dec)
        if len(self._bit_list) < self.length:  # if initialize from '0b001', length will be less than 3
            self._bit_list += ['0'] * (self.length - len(self._bit_list))

    def __setitem__(self, key, value):
        if isinstance(key, slice):
            assert isinstance(value, IcBits), "Right hand side must be IcBits instance."
            assert len(value) == key.start - key.stop + 1, "Assigning bits with different length."
            for idx in range(self.length):
                if key.stop <= idx <= key.start:
                    self._bit_list[idx] = value._bit_list[idx - key.stop]
            # elif isinstance(value, str):
            #     if value[0:2] == "0b":
            #         assert len(value) - 2
        else:
            if isinstance(value, IcBits):
                assert value._bit_list[0] in ['0', '1'], "Assigning a illegal value to IcBits instance."
                self._bit_list[key] = value._bit_list[0]
            elif isinstance(value, str):
                if len(value) == 1:
                    assert value in ['0', '1'], "Assigning a illegal value to IcBits instance."
                    self._bit_list[key] = value
                else:
                    assert value in ["0x0", "0x1", "0b0", "0b1"], "Assigning a illegal value to IcBits instance."
                    self._bit_list[key] = value[-1]
            elif isinstance(value, int):
                assert value in [0, 1], "Assigning a illegal value to IcBits instance."
                self._bit_list[key] = str(value)
        self.sync_value()

    def __getitem__(self, key):
        if isinstance(key, slice):
            ic_stop = key.start    # python start is verilog stop
            ic_start = key.stop    # python stop is verilog start
            py_start = key.stop
            py_end = key.start + 1
            if self.length < (ic_stop + 1):
                # raise Exception("IndexError, the slice index is illegal.")
                self._bit_list = self._bit_list + ['0'] * (ic_stop + 1 - self.length)
            out_bit_list = self._bit_list[py_start: py_end]

            return IcBits(out_bit_list)
        else:
            return IcBits("0b" + self._bit_list[key])

    def __delitem__(self, key):
        pass

    def __eq__(self, other):
        result = False
        if isinstance(other, IcBits):
            result |= self._dec == other._dec
        elif isinstance(other, int):
            result |= self._dec == other
        elif isinstance(other, str):
            if other[0:2] == '0b':
                result |= self._dec == int(other, base=2)
            elif other[0:2] == '0x':
                result |= self._dec == int(other, base=16)
            else:
                result |= self._dec == int(other)
        return result

    def __invert__(self):
        new_bit_list = list()
        for bit in self._bit_list:
            if bit == '0':
                new_bit_list.append('1')
            elif bit == '1':
                new_bit_list.append('0')
        self._bit_list = new_bit_list
        self.sync_value()
        return self

    def __len__(self):
        return self.length

    def __repr__(self):
        return self._hex

    def sync_value(self):
        """
        Sync value of all member if modification is applied on _bit_list
        :return:
        """
        self._hex = list2hex(self._bit_list)
        self._dec = list2dec(self._bit_list)
        self._bin = list2bin(self._bit_list)

    def set_len(self, length):
        self._bit_list += (length - self.length) * ['0']
        self.length = length

    def concat(self, length, *args):
        """
        The input must be InnerData instance.
        """
        result = list()
        for arg in args[0]:
            arg._bit_list.reverse()  # First item is the most significant bit. Inplace operation and need reverse again.
            result += arg._bit_list  # First item is the most significant bit.
            arg._bit_list.reverse()  # restore the endian in case the args will be reused outside concat()
        result.reverse()  # First item is the least significant bit. Correspond with definition of InnerData.bit_list
        # The length may be less than required. Append zeros. These zeros is high bits in verilog.
        result = result + ['0'] * (length - len(result))
        self._bit_list = result

    def set_big(self):
        """
        The default endian in IcBits is little endian.
        If big endian is needed. This method must be called manually.
        :return:
        """
        self.is_lit = False

    def big2lit(self):
        """
        :return:
        """
        if self.length % 8 != 0:
            big_bit_list = self._bit_list + ['0'] * (8 - (self.length % 8))
            big_len = self.length + (8 - (self.length % 8))
        else:
            big_bit_list = self._bit_list
            big_len = self.length

        lit_bit_list = list()
        for i in range(big_len, 0, -8):
            lit_bit_list.extend(big_bit_list[i - 8: i])

        self.is_lit = True

        # If the most significant bit in result is not 0, the length may increase.
        if big_len > self.length and lit_bit_list[-1] == '1':
            self.length = big_len

        self._bit_list = lit_bit_list
        self._dec = list2dec(self._bit_list)
        self._hex = list2hex(self._bit_list)


def bin2list(in_bin):
    bit_list = [bit for bit in in_bin[2:]]
    bit_list.reverse()
    return bit_list


def dec2list(in_dec):
    bit_list = [bit for bit in bin(in_dec)[2:]]
    bit_list.reverse()
    return bit_list


def hex2list(in_hex):
    bit_list = [bit for bit in bin(int(in_hex, base=16))[2:]]
    bit_list.reverse()
    return bit_list


def list2bin(in_bit_list):
    tmp_list = deepcopy(in_bit_list)  # Avoid reverse the input list.
    tmp_list.reverse()
    return "0b" + ''.join(tmp_list)


def list2hex(in_bit_list):
    """
    in_bit_list: a bit list whose 0st item is least significant bit.
    """
    tmp_list = deepcopy(in_bit_list)  # Avoid reverse the input list.
    tmp_list.reverse()
    return hex(int(''.join(tmp_list), base=2))


def list2dec(in_bit_list):
    """
    in_bit_list: a bit list whose 0st item is least significant bit.
    """
    tmp_list = deepcopy(in_bit_list)  # Avoid reverse the input list.
    tmp_list.reverse()
    return int(''.join(tmp_list), base=2)


def print_bit_list(in_bit_list):
    """
    in_bit_list: a bit list whose 0st item is least significant bit.
    """
    tmp_list = deepcopy(in_bit_list)  # Avoid reverse the input list.
    tmp_list.reverse()
    print(hex(int(''.join(tmp_list), base=2)))


def get_uint(i):
    if i[1] == "x":
        return int(i, base=16)
    elif i[1] == "b":
        return int(i, base=2)

