import os
import pdb
from copy import deepcopy
import subprocess
import re
from argparse import ArgumentParser
from pprint import pprint

from icbits import IcBits


# Walking Zeroes.*?FAILURE  ???  walkzero.*?FAILURE.*?offset

KEYWORDS = [
        "INITIALOK",
        "EFUSE_SCCL",
        "SLT_DDR_walkzero -- PASS",
        "SLT_DDR_walkzero -- FAIL",
        "FAILURE.*\!=.*p1.*p2",
        "SLT_DDR_mt_512 -- PASS",
        "SLT_DDR_mt_512 -- FAIL",
        "bit_index.*count",
        "_pg_cnt",
        "ERROR\]\[CORE",
        "FAILURE:\[c\d+\]",
        "g_log_head_arr.*MT[0-9]",
        ]


def get_args():
    arg_parser = ArgumentParser()
    arg_parser.add_argument('--input', required=True)
    arg_parser.add_argument('--output', required=False)
    return arg_parser.parse_args()


def grep_related_lines():
    cmd = "grep -arn -e "
    cmd += " -e ".join([f"'{kw}'" for kw in KEYWORDS])
    cmd += f" '{os.path.abspath(args.input)}'"
    related_lines = subprocess.check_output(cmd, shell=True).decode().split('\n')
    return related_lines


# def grep_and_split(grep_kw, split_kw='\n', sub_kw=None):
#     """
#     哪个CPU的识别关键字：
#         grep_kw = f"grep -n INITIALOK {args.input}"
#         split_kw = '\n'
#     单bit错的识别关键字：
#         grep_kw = f"grep -n 'walkzero.*FAIL' {args.input}"
#         split_kw = '\n'
#     """
#     grep_result = subprocess.check_output(f"grep -n {grep_kw} {args.input}", shell=True).decode()
#     split_result = grep_result.split(split_kw)
#     to_ret = {}
#     for grep_out_line in split_result:
#         if not re.match('^$', grep_out_line):
#             try:
#                 line_no, line_ctt = grep_out_line.strip().split(':')
#             except Exception as e:
#                 print(e)
#                 print(grep_out_line)
#                 exit(1)
#             to_ret.update({int(line_no) - 1: line_ctt})
#     return to_ret


def group_by_cpu_line(in_lines, g_start_kw="INITIALOK"):
    """
    把log按CPU分组切开
    """
    inside_group = False
    to_ret = []
    one_group = None
    for line in in_lines:
        if g_start_kw in line:
            if not inside_group:    # 处理第一个group
                one_group = [line]
            if inside_group:
                to_ret.append(deepcopy(one_group))
                del one_group
                one_group = [line]
            inside_group = True
        if inside_group:
            one_group.append(line)
    to_ret.append(deepcopy(one_group))    # 收最后一个group

    return to_ret


def clean_lines(in_lines: list):
    to_ret = []
    for line in in_lines:
        if re.match('^$', line):    # 跳过空行
            continue
        line = re.sub('\r', '', line)
        # TODO sub ^H in vim
        to_ret.append(line)
    return to_ret


def collect_cpu_info(in_lines: list, a_csv_line: list):
    a_csv_line.append(':'.join(in_lines[0].split(':')[0:2]))


def collect_efuse_info(in_lines: list, a_csv_line: list):
    def decode_efuse(efuse_orig):
        # pdb.set_trace()
        e_o_splited = efuse_orig.split(' ')
        efuse_63_0 = IcBits('0x' + e_o_splited[1] + e_o_splited[0])
        to_ret = []

        for h, l in zip(range(9, 40, 6), range(4, 35, 6)):
            # print(h, ':', l)
            lotid = efuse_63_0[h: l]._dec
            to_ret.append(lotid)

        waferid = efuse_63_0[44: 40]._dec
        to_ret.append(waferid)

        die_x = efuse_63_0[51: 45]._dec * (1 if efuse_63_0[52] == 0 else -1)
        to_ret.append(die_x)

        die_y = efuse_63_0[59: 53]._dec * (1 if efuse_63_0[60] == 0 else -1)
        to_ret.append(die_y)

        return to_ret

    efuse_a_found, efuse_a_found = False, False
    for line in in_lines:
        re_result = re.findall('EFUSE_SCCLA\[0:255\]:\s+=\s+(.*?)EFUSE_SCCLA\[256', line)
        if len(re_result) > 0:
            a = decode_efuse(re_result[0])    # SCCLA lotid0~6, waferid, diex, diey
            a_csv_line.extend([ str(i) for i in a ])
            efuse_a_found = True
        re_result = re.findall('EFUSE_SCCLB\[0:255\]:\s+=\s+(.*?)EFUSE_SCCLB\[256', line)
        if len(re_result) > 0:
            b = decode_efuse(re_result[0])    # SCCLB lotid0~6, waferid, diex, diey
            a_csv_line.extend([ str(i) for i in b ])
            efuse_b_found = True
    if not efuse_a_found:
        a_csv_line.extend(['NA'] * 9)    # SCCLA missing
    if not efuse_b_found:
        a_csv_line.extend(['NA'] * 9)    # SCCLB missing


def collect_walkzero_info(in_lines: list, a_csv_line: list):
    def lh_err(lh_bits, rh_bits):
        if lh_bits != 0:
            return True
        elif rh_bits != 0:
            return False
        else:
            print("Lefthand and Righthand both 0")
            exit(1)

    def get_onehot_idx(in_bits):
        onehot_str = in_bits._bin[2:]
        onehot_list = [bit for bit in onehot_str]
        onehot_list.reverse()
        oh_rev_list = deepcopy(onehot_list)
        for i, b in enumerate(oh_rev_list):
            if b == '1':
                return i

    collecting_fail_detail = False
    err_bit_idx = []
    for line in in_lines:
        if "SLT_DDR_walkzero -- PASS" in line:
            a_csv_line.append("SLT_DDR_walkzero -- PASS")
            a_csv_line.append("NA")
            return
        elif "SLT_DDR_walkzero -- FAIL" in line:
            a_csv_line.append("SLT_DDR_walkzero -- FAIL")
            collecting_fail_detail = True
        elif collecting_fail_detail and "walkzero" in line and "FAILURE" in line:
            # pdb.set_trace()
            re_result = re.findall('walkzero.*?FAILURE.*?(0x\d+) != (0x\d+).*?p1:(0x[0-9|a-f]+), p2:(0x[0-9|a-f]+)', line)
            lh, rh, p1, p2 = re_result[0]
            lh_bits, rh_bits = IcBits(lh.strip()), IcBits(rh.strip())
            p1_bits, p2_bits = IcBits(p1.strip()), IcBits(p2.strip())

            # 确认L错误还是R错误
            if lh_err(lh_bits, rh_bits):
                onehot_offset = get_onehot_idx(lh_bits)
                y = (p2_bits._dec % 64) * 8
            else:
                onehot_offset = get_onehot_idx(rh_bits)
                y = (p1_bits._dec % 64) * 8
            walkzero_bit_idx = onehot_offset + y

            err_bit_idx.append(str(walkzero_bit_idx))    # 一次walkzero fail会跟着若干行FAILURE信息
    a_csv_line.append("&".join(set(err_bit_idx)))    # 把一组bit_idx拼接成一个字符串


def collect_mt512_info(in_lines: list, a_csv_line: list):
    collecting_fail_detail = False
    err_bit_idx = []
    for line in in_lines:
        if "SLT_DDR_mt_512 -- PASS" in line:
            a_csv_line.append("SLT_DDR_mt_512 -- PASS")
            a_csv_line.append("NA")
            return
        elif re.findall('SLT_DDR_mt_512 -- FAIL', line):
            a_csv_line.append('SLT_DDR_mt_512 -- FAIL')
            collecting_fail_detail = True
        elif collecting_fail_detail and re.findall('bit_index = \d+, count = \d+', line):
            re_result = re.findall('bit_index = (\d+), count = \d+', line)
            mt512_bit_idx = re_result[0]
            err_bit_idx.append(mt512_bit_idx)    # 一次mt512 fail会跟着若干行FAILURE信息
    a_csv_line.append("&".join(set(err_bit_idx)))    # 把一组bit_idx拼接成一个字符串


def is_pg_or_fg(lines_of_cpu):
    ta_pg_cnt = None
    for line in lines_of_cpu:
        find_rlt = re.findall('ta_pg_cnt is (\d+).*tb_pg_cnt is (\d+)', line)
        if len(find_rlt) > 0:
            ta_pg_cnt = int(find_rlt[0][0])
            tb_pg_cnt = int(find_rlt[0][1])
            assert ta_pg_cnt == tb_pg_cnt, f"ta_pg_cnt({ta_pg_cnt}) != tb_pg_cnt({tb_pg_cnt})"
    if ta_pg_cnt == 0:
        return "FG"
    else:
        return "PG"


# # TA or TB: Which totem fail {{
def is_ta_or_tb(lines_a_cpu, pg_or_fg, mt512_or_walkzero, a_csv_line: list):
    """
    判断TA/TB会输入一组log行，TODO 这些行并不全，只能给出一个hint，不全
    """
    if mt512_or_walkzero == "mt512":
        re_find_kw = '\[ERROR\]\[CORE(\d+)\]'
    if mt512_or_walkzero == "wzero":
        re_find_kw = 'FAILURE:\[c(\d+)\]'

    def one_line_ta_tb(re_find_kw, line, pg_or_fg="PG"):
        f_r = re.findall(re_find_kw, line)
        if len(f_r) == 1:
            err_core_id = int(f_r[0])
        else:
            return "无法提取到[ERROR][CORExx]"

        if pg_or_fg == "PG":
            return "TB" if err_core_id < 48 else "TA"
        elif pg_or_fg == "FG":
            return "TB" if err_core_id < 64 else "TA"

    # 捞出相关的行
    lines = []
    for l in lines_a_cpu:
        # pdb.set_trace()
        if re.search(re_find_kw, l):
            lines.append(l)
    if len(lines) == 0:
        a_csv_line.append("NA")

    tmp_list = []
    for line in lines:
        tmp_list.append(one_line_ta_tb(re_find_kw, line, pg_or_fg))

    a_csv_line.append('&'.join(set(tmp_list)))

# # Which totem fail }}


def collect_temperature(lines_a_cpu, a_csv_line):
    for line in lines_a_cpu:
        # if "g_log_head_arr" in line:
        re_find_r = re.findall("g_log_head_arr.*(MT\d+)", line)
        if len(re_find_r) > 0:
            a_csv_line.append(re_find_r[0])
            return
    a_csv_line.append("NA")


# # 打印 最近的一次 mt_512=>TA/TB 的信息
# # 打印 394 错误 (通过 mt_512 的，直接输出结果行)
# # 打印 walkzero & mt512 是否都检查到394bit出错


if __name__ == "__main__":
   
    args = get_args()

    PRINT_LINES = []

    grep_out_lines = grep_related_lines()
    grep_lines_each_cpu = group_by_cpu_line(grep_out_lines)

    col_names  = "FILE_PATH,CPU,"
    col_names += "TA_LOTID[0],TA_LOTID[1],TA_LOTID[2],TA_LOTID[3],TA_LOTID[4],TA_LOTID[5],TA_WAFERID,TA_X,TA_Y,"
    col_names += "TB_LOTID[0],TB_LOTID[1],TB_LOTID[2],TB_LOTID[3],TB_LOTID[4],TB_LOTID[5],TB_WAFERID,TB_X,TB_Y,"
    col_names += "WALKZERO,WALKZERO_BIT_IDX,MT_512,MT_512_BIT_IDX,"
    col_names += "TA/TB_MT512,TA/TB_WZERO,"
    col_names += "TEMPERATURE"
    col_names += "\n"
    csv_rows = [col_names]
    for grep_lines_a_cpu in grep_lines_each_cpu:
        a_csv_row_l = [os.path.abspath(args.input)]

        grep_lines_a_cpu = clean_lines(grep_lines_a_cpu)

        collect_cpu_info(grep_lines_a_cpu, a_csv_row_l)

        collect_efuse_info(grep_lines_a_cpu, a_csv_row_l)

        collect_walkzero_info(grep_lines_a_cpu, a_csv_row_l)

        collect_mt512_info(grep_lines_a_cpu, a_csv_row_l)

        pg_fg = is_pg_or_fg(grep_lines_a_cpu)
        is_ta_or_tb(grep_lines_a_cpu, pg_fg, mt512_or_walkzero="mt512", a_csv_line=a_csv_row_l)
        is_ta_or_tb(grep_lines_a_cpu, pg_fg, mt512_or_walkzero="wzero", a_csv_line=a_csv_row_l)

        collect_temperature(grep_lines_a_cpu, a_csv_row_l)

        csv_rows.append(','.join(a_csv_row_l) + '\n')
        del a_csv_row_l

    # pprint(csv_rows)

    if args.output is None:
        input_abs = os.path.abspath(args.input)
        output_file_name = re.sub('\.(txt|log)', '.csv', input_abs)
        with open(output_file_name, 'w') as fout:
            for r in csv_rows:
                fout.write(r)

